﻿using AutoMapper;
using ClosedXML.Excel;
using Directory.framework;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Controllers
{
    ///<summary>Контроллер для классификатора диагностических инструментальных методов исследования</summary>
    public class DiagnInstrumMethodsController : JsonController
    {
        private readonly ILogger<DiagnInstrumMethodsController> _logger;
        private readonly IMapper _mapper;
        private readonly IDiagnInstrumMethodsServices _diagnInstrumMethodsServices;
        private readonly ISectionDIMServices _sectionDIMServices;
        private readonly IViewStudyServices _viewStudyServices;

        private readonly string name = "Справочник диагностических интрсументальных методов";

        public DiagnInstrumMethodsController(
            ILogger<DiagnInstrumMethodsController> logger,
            IDiagnInstrumMethodsServices diagnInstrumMethodsServices,
            IMapper mapper,
            ISectionDIMServices sectionDIMServices,
            IViewStudyServices viewStudyServices)
        {
            _logger = logger;
            _diagnInstrumMethodsServices = diagnInstrumMethodsServices;
            _mapper = mapper;
            _sectionDIMServices = sectionDIMServices;
            _viewStudyServices = viewStudyServices;
        }


        [AllowAnonymous]
        [Route("DiagnInstrumMethods")]
        public ActionResult DiagnInstrumMethods(DiagnInstrumMethodsFilter filter)
        {
            if (filter is null) { throw new ArgumentNullException("filter is null", nameof(filter)); }

            _logger.LogInformation($"{name}: Выдача данных");

            try
            {
                var DIM = _diagnInstrumMethodsServices.FilterDiagnInstrumMethods(filter);

                filter.DIMethods = DIM;

                return View(filter);
            }
            //При ошибки может быть дисконект с сервером и тд
            catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return View("BadRequest"); }
            catch (Exception e) { return StatusCode(500, e.Message); }
        }



        public ActionResult Create()
        {
            _logger.LogInformation($"{name}: Попытка создания справочника");

            ViewBag.SectionList = _sectionDIMServices.GetSectionWithCreate();
            ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DIMViewModel model)
        {
            if (!Validation(model)) { return View("Create", model); }

            _logger.LogInformation($"{name}: Создание справочника {model.Id}");

            DiagnInstrumMethods DIM = _mapper.Map<DiagnInstrumMethods>(model);

            try
            {
                _diagnInstrumMethodsServices.CreateDIM(DIM);

                return RedirectToAction(nameof(Index));
            }
            catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return View("BadRequest"); }
            catch (Exception e) { return StatusCode(500, e.Message); }
        }



        public ActionResult Edit(int id)
        {
            _logger.LogInformation($"{name}: Попытка изменения справочника {id}");

            ViewBag.SectionList = _sectionDIMServices.GetSectionWithCreate();
            return View(_diagnInstrumMethodsServices.GetDIMById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, DIMViewModel model)
        {
            if (!Validation(model)) { return View("Edit", model); }

            _logger.LogInformation($"{name}: Редактирование справочника {model.Id}");

            DiagnInstrumMethods DIM = _mapper.Map<DiagnInstrumMethods>(model);

            try
            {

                _diagnInstrumMethodsServices.EditDIM(DIM);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                _logger.LogInformation($"{name}: Удаление справочника {id}");

                _diagnInstrumMethodsServices.DeleteDIM(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        [Route("DiagnInstrumMethods/API")]
        public ActionResult API() { return Ok("{Сайт} /api/Token/GetDIMByToken?token= {Ваш TOKEN}"); }

        [HttpGet]
        [Route("DiagnInstrumMethods/Excel")]
        public IActionResult Excel()
        {
            var datas = _diagnInstrumMethodsServices.FilterDiagnInstrumMethods(null).OrderBy(e => e.SectionCode).OrderBy(e => e.CodeStudy);

            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("directory");
            var currentRow = 1;
            worksheet.Cell(currentRow, 1).Value = "Вид исследования";
            worksheet.Cell(currentRow, 2).Value = "Код раздела";
            worksheet.Cell(currentRow, 3).Value = "Название раздела";
            worksheet.Cell(currentRow, 4).Value = "Порядковый номер";
            worksheet.Cell(currentRow, 5).Value = "Код исследования";
            worksheet.Cell(currentRow, 6).Value = "Название исследования";

            foreach (var data in datas)
            {
                currentRow++;
                worksheet.Cell(currentRow, 1).Value = data.ViewStudy;
                worksheet.Cell(currentRow, 2).Value = data.SectionCode;
                worksheet.Cell(currentRow, 3).Value = data.SectionName;
                worksheet.Cell(currentRow, 4).Value = data.OrdinalNumber;
                worksheet.Cell(currentRow, 5).Value = data.CodeStudy;
                worksheet.Cell(currentRow, 6).Value = data.NameStude;
            }

            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            var content = stream.ToArray();

            return File(
                content,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "DiagnInstrumMethods.xlsx");
        }

        private bool Validation(DIMViewModel model)
        {
            if (model is null) { throw new ArgumentNullException("DIM is null", nameof(model)); }

            if (string.IsNullOrEmpty(model.SectionCode) || model.SectionCode == null)
            {
                ModelState.AddModelError("Invalid SectionName", $"*{model.SectionCode}* Раздел обязателен!");

                ViewBag.SectionList = _sectionDIMServices.GetSectionWithCreate();

                return false;
            }
            else if (string.IsNullOrEmpty(model.NameStude) || model.NameStude == null)
            {
                ModelState.AddModelError("Invalid NameStude", $"*{model.NameStude}* Название исследования обязательно!");

                ViewBag.SectionList = _sectionDIMServices.GetSectionWithCreate();

                return false;
            }

            return true;
        }


    }
}
