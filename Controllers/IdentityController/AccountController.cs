﻿using AutoMapper;
using Directory.Data.Common;
using Directory.framework;
using Directory.Models;
using Directory.Models.Identity;
using Directory.Models.ModelsView;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Controllers
{
	///<summary>Перечисление описывающее тип символа</summary>
	enum Symbols
	{
		empty = 0,
		letter = 1,
		digit = 2,
		symbol = 3
	}

	public class AccountController : JsonController
	{
		private readonly UserManager<Person> _personManager;
		private readonly SignInManager<Person> _signInManager;
		private readonly ICurrentPersonServices _currentPersonSerivces;
		private readonly ILogger<AccountController> _logger;

		public AccountController(
		UserManager<Person> personManager,
		ICurrentPersonServices currentPersonSerivces,
		SignInManager<Person> signInManager,
		ILogger<AccountController> logger)
		{
			_personManager = personManager;
			_currentPersonSerivces = currentPersonSerivces;
			_signInManager = signInManager;
			_logger = logger;
		}


		[HttpGet]
		[AllowAnonymous]
		public IActionResult Login(string returnUrl = null)
		{
			return View(new LoginViewModel { ReturnUrl = returnUrl });
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginViewModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, false);

					if (result.Succeeded)
					{
						// проверяем, принадлежит ли URL приложению
						if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
						{
							return Redirect(model.ReturnUrl);
						}
						else
						{
							return RedirectToAction("Index", "Home");
						}
					}
					else if (result.IsLockedOut)
					{
						TempData["AlertResult"] = JsonConvert.SerializeObject(Result.Failure("Ваш аккаунт заблокирован или недоступен!"));
					}
					else
					{
						TempData["AlertResult"] = JsonConvert.SerializeObject(Result.Failure("Неправильный логин и (или) пароль"));
					}
				}
				catch (Exception e)
				{
					_logger.LogError($"Ошибка входа: {e.Message}");
				}
			}
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Logout()
		{
			// удаляем аутентификационные куки
			await _signInManager.SignOutAsync();
			return RedirectToAction("Index", "Home");
		}

		public IActionResult ChangePassword()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> SavePassword(ChangePasswordViewModel model)
		{
			if (ModelState.IsValid)
			{
				//Проверяю пароль на надежность
				if (AusterityPassword(model.NewPassword) < 40)
				{ TempData["AlertResult"] = JsonConvert.SerializeObject(Result.Failure("Пароль слишком слабый")); }
				else
				{
					var user = await _personManager.Users.SingleOrDefaultAsync(u => u.Id == _currentPersonSerivces.Id);

					if (user != null)
					{
						IdentityResult result = await _personManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

						if (result.Succeeded)
						{
							return RedirectToAction("Index", "Home");
						}
						else
						{
							foreach (var error in result.Errors)
							{
								_logger.LogError($"Ошибка обновления пароля: {error.Description}");
							}
							TempData["AlertResult"] = JsonConvert.SerializeObject(Result.Failure("Текущий пароль введен неверно"));
						}
					}
					else
					{
						TempData["AlertResult"] = JsonConvert.SerializeObject(Result.Failure("Пользователь не найден"));
					}

				}
			}
			return RedirectToAction("ChangePassword", "Account");
		}

		///<summary>Возвращает балл, оценивающий надежность пароля</summary>
		///<param name="password">Строка пароля</param>
		private static int AusterityPassword(string password)
		{
			if (string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password)) { throw new ArgumentNullException("Строка является пустой", nameof(password)); }
			if (password.Length < 8 || password.Length > 30) { throw new ArgumentOutOfRangeException("Длина пароля слишком маленькая или слишком большая", nameof(password)); }

			float _equability = 100;
			int _countLettersUp = 0, _countLettersLower = 0, _countLetters = 0;
			int _countDigit = 0;
			int _countSymbols = 0;
			Symbols backTypeSymbol = Symbols.empty; //Создаем экземпляр перечисления типа символа

			List<char> _repeatSymbols = new(); //Список повторных символов

			int score = 1;

			//Первым делом подсчитываем количество символов, их разновидновть и соотношение
			//===========================================================================

			//Проходим по символам пароля
			foreach (var symbol in password)
			{
				if (password.IndexOf(symbol, score) != -1) { _repeatSymbols.Add(symbol); }

				//Если текущий символ буква, инкрементируем счетчик количества букв.
				if (char.IsLetter(symbol))
				{
					_countLetters++;
					if (symbol < 65 || symbol > 122) { throw new ArgumentException(nameof(password), "Пароль может содержать только латинские буквы"); }

					//Если прошлый и текущий символ буква, то вычитаем процент от одного символа равномерности по отношению к 100%
					if (backTypeSymbol.Equals(Symbols.letter)) { _equability -= Percentage(1, 100, password.Length); }
					backTypeSymbol = Symbols.letter;

					//Определяем количество букв в верхнем регистре и нижнем
					if (char.IsUpper(symbol)) { _countLettersUp++; } else { _countLettersLower++; }
				}

				//Если текущий символ цифра, инкрементируем счетчик количества цифр.
				else if (char.IsDigit(symbol))
				{
					_countDigit++;
					//Если прошлый и текущий символ цифра, то вычитаем процент от одного символа равномерности по отношению к 100%
					if (backTypeSymbol.Equals(Symbols.digit)) { _equability -= Percentage(1, 100, password.Length); }
					backTypeSymbol = Symbols.digit;
				}

				//Если текущий символ символный тип, инкрементируем счетчик количества символов.
				else if (char.IsPunctuation(symbol) || char.IsSymbol(symbol))
				{
					_countSymbols++;
					//Если прошлый и текущий символ символный тип, то вычитаем процент от одного символа равномерности по отношению к 100%
					if (backTypeSymbol.Equals(Symbols.symbol)) { _equability -= Percentage(1, 100, password.Length); }
					backTypeSymbol = Symbols.symbol;
				}

				score++;
			}

			//Процентное соотношение букв, цифр, и символов в пароле
			(float procLetters, float procDigits, float procSymbols) _relation =
					(Percentage(_countLetters, 100, password.Length),
					 Percentage(_countDigit, 100, password.Length),
					 Percentage(_countSymbols, 100, password.Length));

			//Вторым пунктом определяем балл исходя из подсчета
			//===========================================================================

			float relation = 0,   //Балл за соотношение символов (от общего балла 30%)
			register = 10;      //Балл за равномерность регистра (от общего балла 10%)

			//===========================================================================
			//Находим соотношение символов, и присуждаем им балл (от общего балла составляет не более 30%)

			//Находим соотношение символов: 40 на 40 на 20 = идеально. Соотношение от всех балоов будет занимать 30%.
			relation = Math.Abs(_relation.procLetters - Math.Abs(_relation.procLetters - (_relation.procLetters == 0 ? 0 : 40))
							+ _relation.procDigits - Math.Abs(_relation.procDigits - (_relation.procDigits == 0 ? 0 : 40))
							+ _relation.procSymbols - Math.Abs(_relation.procSymbols - (_relation.procSymbols == 0 ? 0 : 20)));

			//Определяю процент соотношения символов к общему баллу пароля, как 30%.
			relation = (relation / 30f) * 10;

			//===========================================================================
			//Находим соотношение символов с нижним и верхним регистром, и присуждаем балл (не более 10%)

			//Проверяем, содержаться ли буквы в пароле
			if (_relation.procLetters > 0)
			{
				//Находим процентное количество букв нижнего регистра в пароле
				float countProcLetterLower = Percentage(_countLettersLower, 100, _countLetters);

				//Находим процентное количество букв верхнего регистра в пароле
				float countProcLetterUp = Percentage(_countLettersUp, 100, _countLetters);

				//Отнмаем от общего балла соотношений, разницу между буквами нижнего и верхнего регистра.
				//(Чем больше разность регистров - чем меньше общий балл).
				register -= Math.Abs(countProcLetterUp - countProcLetterLower) / 10;
			}
			else { register = 0; }

			//===========================================================================
			//Находим длину пароля, и присуждаем ему балл (от общего балла составляет не более 30%)

			//===========================================================================
			//Находим равномерность типов символов, присуждаем балл (от общего балла составляет не более 30% )

			_equability = _equability / 3;

			//Чтобы пароль не выглядил слабым, из за равномерности, уравновесим балл.
			_equability = (_equability > 30 ? 30 : _equability + (10 / _equability));

			//===========================================================================

			//Суммируем соотношение символов, соотношение регистра букв, длины пароля и равномерности типов символов.
			float ball = relation + register + password.Length + _equability;

			//Если в пароле не содержиться какой-либо из типов символов, то отнимаем баллы.
			if (_relation.procLetters == 0) { ball -= 10; }
			if (_relation.procDigits == 0) { ball -= 5; }
			if (_relation.procSymbols == 0) { ball -= 3; }

			//Отнимаем от общего балла количество повторных символов, и возвращаем общий балл.
			return (int)ball - _repeatSymbols.Count;
		}

		///<summary>Возвращает процент, который определяет, сколько составляет <paramref name="count"/> процентов от общей длины строки</summary>
		///<param name="count">Количество символов</param>
		///<param name="howPercent">От скольки процентов считать полную длину пароля</param>
		///<param name="lenght">Длина пароля</param>
		private static float Percentage(float count, float howPercent, int lenght) { return count / lenght * howPercent; }

	}
}
