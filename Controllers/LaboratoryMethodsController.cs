﻿using AutoMapper;
using ClosedXML.Excel;
using Directory.framework;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace Directory.Controllers
{
	///<summary>Контроллер для классификатора лабораторных методов исследования</summary>
	public class LaboratoryMethodsController : JsonController
	{
		private readonly ILogger<LaboratoryMethodsController> _logger;
		private readonly IMapper _mapper;
		private readonly ILaboratoryMethodsServices _laboratoryMethodsServices;
		private readonly ISectionLMServices _sectionLMServices;
		private readonly IViewStudyServices _viewStudyServices;

		private readonly string name = "Справочник лабораторных методов исследования";

		public LaboratoryMethodsController(
		ILogger<LaboratoryMethodsController> logger,
			ILaboratoryMethodsServices laboratoryMethodsServices,
			IMapper mapper,
			ISectionLMServices sectionLMServices,
			IViewStudyServices viewStudyServices)
		{
			_logger = logger;
			_laboratoryMethodsServices = laboratoryMethodsServices;
			_mapper = mapper;
			_sectionLMServices = sectionLMServices;
			_viewStudyServices = viewStudyServices;
		}

		[AllowAnonymous]
		[Route("LaboratoryMethods")]
		public ActionResult LaboratoryMethods(LaboratoryMethodsFilter filter)
		{
			if (filter is null) { throw new ArgumentNullException("filter is null", nameof(filter)); }

			_logger.LogInformation($"{name}: Выдача данных");

			try
			{
				var LM = _laboratoryMethodsServices.FilterLaboratoryMethods(filter);

				filter.LMMethods = LM;

				return View(filter);
			}
			//При ошибки может быть дисконект с сервером и тд
			catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return View("BadRequest"); }
			catch (Exception e) { return StatusCode(500, e.Message); }
		}




		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания справочника");

			ViewBag.SectionList = _sectionLMServices.GetSectionWithCreate();
			ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(LMViewModel model)
		{
			if (!Validation(model)) { return View("Create", model); }

			_logger.LogInformation($"{name}: Создание справочника {model.Id}");

			LaboratoryMethods LM = _mapper.Map<LaboratoryMethods>(model);

			try
			{
				_laboratoryMethodsServices.CreateLM(LM);

				return RedirectToAction(nameof(Index));
			}
			catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return View("BadRequest"); }
			catch (Exception e) { return StatusCode(500, e.Message); }
		}



		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения справочника {id}");

			ViewBag.SectionList = _sectionLMServices.GetSectionWithCreate();
			ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

			return View(_laboratoryMethodsServices.GetLMById(id));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(LMViewModel model)
		{
			if (!Validation(model)) { return View("Edit", model); }

			_logger.LogInformation($"{name}: Редактирование справочника {model.Id}");

			LaboratoryMethods LM = _mapper.Map<LaboratoryMethods>(model);

			try
			{

				_laboratoryMethodsServices.EditLM(LM);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}



		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id)
		{
			try
			{
				_logger.LogInformation($"{name}: Удаление справочника {id}");

				_laboratoryMethodsServices.DeleteLM(id);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		[HttpGet]
		[Route("LaboratoryMethods/API")]
		public ActionResult API() { return Ok("{Сайт} /api/Token/GetLMByToken?token= {Ваш TOKEN}"); }

		[HttpGet]
		[Route("LaboratoryMethods/Excel")]
		public IActionResult Excel()
		{
			var datas = _laboratoryMethodsServices.FilterLaboratoryMethods(null).OrderBy(e => e.SectionCode).OrderBy(e => e.CodeStudy);

			var workbook = new XLWorkbook();
			var worksheet = workbook.Worksheets.Add("directory");
			var currentRow = 1;
			worksheet.Cell(currentRow, 1).Value = "Вид исследования";
			worksheet.Cell(currentRow, 2).Value = "Код раздела";
			worksheet.Cell(currentRow, 3).Value = "Название раздела";
			worksheet.Cell(currentRow, 4).Value = "Порядковый номер";
			worksheet.Cell(currentRow, 5).Value = "Код исследования";
			worksheet.Cell(currentRow, 6).Value = "Название исследования";

			foreach (var data in datas)
			{
				currentRow++;
				worksheet.Cell(currentRow, 1).Value = data.ViewStudy;
				worksheet.Cell(currentRow, 2).Value = data.SectionCode;
				worksheet.Cell(currentRow, 3).Value = data.SectionName;
				worksheet.Cell(currentRow, 4).Value = data.OrdinalNumber;
				worksheet.Cell(currentRow, 5).Value = data.CodeStudy;
				worksheet.Cell(currentRow, 6).Value = data.NameStude;
			}

			var stream = new MemoryStream();
			workbook.SaveAs(stream);
			var content = stream.ToArray();

			return File(
				content,
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				"LaboratoryMethods.xlsx");
		}

		private bool Validation(LMViewModel model)
		{
			if (model is null) { throw new ArgumentNullException("DIM is null", nameof(model)); }

			if (string.IsNullOrEmpty(model.SectionCode) || model.SectionCode == null)
			{
				ModelState.AddModelError("Invalid SectionName", $"*{model.SectionCode}* Раздел обязателен!");

				ViewBag.SectionList = _sectionLMServices.GetSectionWithCreate();

				ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

				return false;
			}
			else if (string.IsNullOrEmpty(model.NameStude) || model.NameStude == null)
			{
				ModelState.AddModelError("Invalid NameStude", $"*{model.NameStude}* Название исследования обязательно!");

				ViewBag.SectionList = _sectionLMServices.GetSectionWithCreate();

				ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

				return false;
			}
			else if (string.IsNullOrEmpty(model.ViewStudy) || model.ViewStudy == null)
			{
				ModelState.AddModelError("Invalid ViewStudy", $"*{model.ViewStudy}* Вид исследования обязательно!");

				ViewBag.SectionList = _sectionLMServices.GetSectionWithCreate();

				ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

				return false;
			}

			return true;
		}
	}
}
