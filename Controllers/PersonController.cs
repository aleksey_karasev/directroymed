﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using Directory.Models.Filters;
using Directory.Services;
using Directory;
using Directory.Services.interfaces;
using Directory.Models.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Directory.framework;
using Newtonsoft.Json;
using Directory.Data.Common;
using Directory.Models.ModelsView;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Directory.Mails;
using Microsoft.Extensions.Logging;

namespace testMVC.Controllers
{
	[Authorize(Roles = "Администратор")]
	public class PersonController : JsonController
	{
		private readonly ILogger<PersonController> _logger;
		private readonly UserManager<Person> _userManager;
		private readonly IPersonServices _personServices;
		private readonly IMapper _mapper;
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;
		private ITokenServices _token;

		public PersonController(
			ILogger<PersonController> logger,
			UserManager<Person> userManager,
			IPersonServices personServices,
			IMapper mapper,
			IUnitOfWorkFactory unitOfWorkFactory,
			ITokenServices token)
		{
			_logger = logger;
			_userManager = userManager;
			_personServices = personServices;
			_mapper = mapper;
			_unitOfWorkFactory = unitOfWorkFactory;
			_token = token;
		}

		[Route("Person/{id?}/{firstName?}/{lastName?}/{userName?}")]
		public ActionResult Index(PersonFilter filter)
		{
			if (filter is null) { throw new ArgumentNullException("filter is null", nameof(filter)); }

			_logger.LogInformation($"Пользователи: Выдача данных");

			try
			{
				var persons = _personServices.FilterPerson(filter);

				//Показываю сначало заявки, потом пользователей незаблокированных, после всех остальных
				filter.persons = persons.OrderBy(loc => loc.LockoutEnabled == true).OrderBy(e => e.isApplicationForm == false).ToList();

				return View(filter);
			}
			//При ошибки может быть дисконект с сервером и тд
			catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return View("BadRequest"); }
			catch (Exception e) { return StatusCode(500, e.Message); }

		}
		public ActionResult Details(int id)
		{
			return View();
		}

		[AllowAnonymous]
		[Route("Person/Create/")]
		[HttpGet]
		public IActionResult Create()
		{
			_logger.LogInformation($"Пользователи: попытка создания нового и заполнение анкеты");

			return View();
		}

		[AllowAnonymous]
		[Route("Person/Save")]
		[HttpPost]
		public async Task<ActionResult> Save(PersonView createPers)
		{
			try
			{
				if (createPers is null) { ModelState.AddModelError("Name", "Профиль пустой"); }

				if (createPers.FirstName.Where(let => !char.IsLetter(let)).Count() > 0 || createPers.FirstName.Length > 25) { ModelState.AddModelError("Name", "Некорректное имя"); }

				if (createPers.LastName.Where(let => !char.IsLetter(let)).Count() > 0 || createPers.LastName.Length > 30) { ModelState.AddModelError("LastName", "Некорректная фамилия"); }

				if (!string.IsNullOrEmpty(createPers.MiddleName))
				{
					if (createPers.MiddleName.Where(let => !char.IsLetter(let)).Count() > 0 || createPers.MiddleName.Length > 30) { ModelState.AddModelError("MiddleName", "Некорректное отчество"); }
				}

				if (createPers.NameSystem.Length > 40) { ModelState.AddModelError("NameSystem", "Название системы слишком длинное"); }

				if (createPers.PhoneNumber.Where(let => !char.IsDigit(let)).Count() > 0 || createPers.PhoneNumber.Length > 30 || createPers.PhoneNumber.Length < 6) { ModelState.AddModelError("PhoneNumber", "Некорректный номер телефона, можно использовать только цифры"); }

				if (createPers.Grounding.Length > 255) { ModelState.AddModelError("Grounding", "Обоснование слишком длинное"); }

				if (ModelState.IsValid)
				{

					//Получаю доступные роли
					List<PersonRole> GetUserRoles()
					{
						var userRoles = new List<PersonRole>();

						foreach (var role in createPers.RoleId)
						{
							userRoles.Add(new PersonRole { RoleId = role.Id });
						}

						return userRoles;
					}
					//Если пользователь еще не создан, синхронизируем его с ролью и добавляем
					if (createPers.Id == null)
					{

						Person persModels = new()
						{
							PersonRole = GetUserRoles()
						};

						string id = persModels.Id;

						persModels = _mapper.Map<Person>(createPers);

						persModels.Id = id;

						//Нам вообще не нужен UserName но Web API не создает без него данные
						persModels.UserName = id;	

						_token = new TokenServices(_unitOfWorkFactory);

						//Создаем токин и заполняем его индекс.
						persModels.TokenId = _token.CreateToken(id);

						await _userManager.CreateAsync(persModels);
						await _userManager.SetLockoutEnabledAsync(persModels, false);

						//Текст сообщения может измениться, исходя от того, одобрили ли заявку
						string message = ""; string pass = "";

						pass = GetPassword(12).Password;
						await _userManager.AddPasswordAsync(persModels, pass);
						message = $"Пользователь успешно активирован для медицинских справочников.\nВам выдан токен, через который вы можете получить данные любого справочника, через API на сайте.\n Ваш токен: {_token.GetTokenById(persModels.TokenId).Tokens}";

						//Отправляется сообщение пользователю
						new Mail("minzdravkr@mail.ru").Send("rnaXB5EqBW7tBhJttav7", new Mail(persModels.Email), message, "Данные для входа");

						_logger.LogInformation($"Пользователи: создание анкеты и пользователя {createPers.Id}");

					}
					//Если создан, то обновляем пользователя
					else
					{
						Person user = await _userManager.Users.Include(u => u.PersonRole).SingleOrDefaultAsync(u => u.Id == createPers.Id);

						user.LastName = createPers.LastName;
						user.FirstName = createPers.FirstName;
						user.Email = createPers.Email;
						user.Grounding = createPers.Grounding;
						user.MiddleName = createPers.MiddleName;
						user.PhoneNumber = createPers.PhoneNumber;
						user.PersonRole = GetUserRoles();

						if (createPers.LockoutEnabled) { await BlockUser(user.Id); } else { await ActivateUser(user.Id); }

						await _userManager.UpdateAsync(user);

						_logger.LogInformation($"Пользователи: Редактирование пользователя или анкеты {createPers.Id}");

					}
				}

			}
			//При ошибки может быть дисконект с сервером и тд
			catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return RedirectToAction("Home", "Error"); }
			catch (Exception e) { return StatusCode(500, e.Message); }

			if (ModelState.IsValid) { return RedirectToAction("Index", "Home"); }
			else { ViewBag.BadRequestMessage = ModelState.Values; return View("Create"); }

		}

		[Route("Person/Edit/{id?}")]
		public ActionResult Edit(string id)
		{
			if (string.IsNullOrWhiteSpace(id))
			{
				ModelState.AddModelError("Invalid SectionName", $"*Айди обязательно!");

				ViewBag.BadRequestMessage = "Person id is NULL";

				return View("Edit", id);
			}

			_logger.LogInformation($"Пользователи: Редактирование пользователя {id}");

			return View(_personServices.GetPersonById(id));
		}

		[Route("Person/InfoPerson/{id}")]
		public ActionResult InfoPerson(string id)
		{
			_logger.LogInformation($"Пользователи: Подробности пользователя {id}");

			return View(_personServices.GetPersonById(id));
		}

		[Route("Person/ResetPassword/{userId}")]
		public async Task<IActionResult> ResetPassword(string userId)
		{
			if (string.IsNullOrWhiteSpace(userId))
			{
				ModelState.AddModelError("Invalid SectionName", $"*Айди обязательно!");

				ViewBag.BadRequestMessage = "Person id is NULL";

				return View("Edit", userId);
			}

			_logger.LogInformation($"Пользователи: сброс пароля пользователя: {userId}");

			if (userId != null)
			{
				string pass = GetPassword(12).Password;

				await _personServices.ResetPassword(userId, pass);

				TempData["AlertResult"] = JsonConvert.SerializeObject(Result.Failure($"Пароль успешно обновлен до {pass}"));
			}

			return RedirectToAction(nameof(Index));
		}

		[HttpPost]
		public async Task<IActionResult> BlockUser(string userId)
		{
			if (userId == null)
				return Ok("Ошибка: не удалось выполнить действие");

			_logger.LogInformation($"Пользователи: блокировка пользователя: {userId}");

			var tokenByPerson = _token.GetTokenByPersonId(userId);

			await _personServices.BlockUser(userId);

			tokenByPerson._dateEnd = DateTime.Now;

			_token.EditToken(tokenByPerson);

			return Ok("Пользователь успешно заблокирован!");
		}

		[HttpPost]
		public async Task<IActionResult> ActivateUser(string userId)
		{
			if (userId == null)
				return Ok("Ошибка: не удалось выполнить действие");

			_logger.LogInformation($"Пользователи: активация пользователя: {userId}");

			await _personServices.ActivateUser(userId);

			var tokenByPerson = _token.GetTokenByPersonId(userId);

			tokenByPerson._dateEnd = new DateTime();

			_token.EditToken(tokenByPerson);

			return Ok("Пользователь успешно разблокирован!");
		}


		#region "Внутренние функции"

		///<summary>Проверяю пароль на прочность</summary>
		///<param name="length">Длина пароля</param>
		private static (string Password, int[] CharsCount) GetPassword(int length)
		{
			if (length <= 0) { throw new ArgumentOutOfRangeException(nameof(length), "Длина пароля не может быть представлена отрицательным числом или нулем."); }

			Random rnd = new Random();

			char[][] chars = {
			new char[] { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' },
			new char[] { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' },
			new char[] { '0','1','2','3','4','5','6','7','8','9' },
			new char[] { '~', '!', '?', '[', ']', '(', ')', '{', '}', '@', '#', '$', '%', '&', '*', '^' }};

		generatePassword:
			string password = "";
			int[] charsCount = new int[chars.Length];
			int currentIndex = 0, currentGroup = -1, prevGroup = -1, prevPrevGroup = -1;
			char currentLetter, prevLetter = (char)0;

			do
			{
				do { currentGroup = rnd.Next(0, chars.Length); } while (currentGroup == prevGroup && currentGroup == prevPrevGroup);
				do { currentLetter = chars[currentGroup][rnd.Next(0, chars[currentGroup].Length)]; } while (prevLetter == currentLetter);

				password += currentLetter;

				charsCount[currentGroup]++;
				prevLetter = currentLetter;
				prevPrevGroup = prevGroup;
				prevGroup = currentGroup;

			} while (++currentIndex < length);

			foreach (int count in charsCount) { if (count < 2) { goto generatePassword; } }

			return (password, charsCount);
		}

		#endregion
	}
}
