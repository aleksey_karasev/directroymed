﻿using AutoMapper;
using Directory.framework;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Directory.Controllers
{
	///<summary>Контроллер для раздела классификатора диагностических инструментальных методов исследования</summary>
	public class SectionDIMController : JsonController
	{
		private readonly ILogger<SectionDIMController> _logger;
		private readonly IMapper _mapper;
		private readonly ISectionDIMServices _sectionDIMServices;

		private readonly string name = "Раздел диагностических интрсументальных методов";

		public SectionDIMController(ISectionDIMServices sectionDIMServices, IMapper mapper, ILogger<SectionDIMController> logger)
		{
			_sectionDIMServices = sectionDIMServices; _mapper = mapper; _logger = logger;
		}

		#region "Get"

		[Route("SectionDIM")]
		public ActionResult SectionDIM()
		{
			_logger.LogInformation($"{name}: Выдача данных");

			return View(_sectionDIMServices.GetSection());
		}

		#endregion

		#region "Create"

		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания раздела");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SectionDiagnInstrum SDI)
		{
			if (!Validation(SDI)) { return View("Create", SDI); }

			_logger.LogInformation($"{name}: Создание раздела {SDI.id}");

			try
			{
				_sectionDIMServices.CreateSection(SDI);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		#region "Edit"

		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения раздела {id}");

			var DIM = _sectionDIMServices.GetSectionById(id);

			return View(DIM);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(SectionDiagnInstrum sectionDIM)
		{
			if (!Validation(sectionDIM)) { return View("Edit", sectionDIM); }

			_logger.LogInformation($"{name}: Редактирование раздела {sectionDIM.id}");

			try
			{
				_sectionDIMServices.EditSection(sectionDIM);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		private bool Validation(SectionDiagnInstrum sectionDIM)
		{
			if (sectionDIM is null) { throw new ArgumentNullException("DIM is null", nameof(sectionDIM)); }

			if (string.IsNullOrEmpty(sectionDIM.Name) || sectionDIM.Name == null)
			{
				ModelState.AddModelError("Invalid sectionDIM", $"*{sectionDIM.Name}* Название обязательно!");

				return false;
			}

			return true;
		}
	}
}
