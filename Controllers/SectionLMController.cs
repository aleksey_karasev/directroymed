﻿using AutoMapper;
using Directory.framework;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Directory.Controllers
{
	///<summary>Контроллер для раздела классификатора лабораторных методов исследования</summary>
	public class SectionLMController : JsonController
	{
		private readonly ILogger<SectionLMController> _logger;
		private readonly IMapper _mapper;
		private readonly ISectionLMServices _sectionLMServices;

		private readonly string name = "Раздел лабораторных методов исследования";

		public SectionLMController(ISectionLMServices sectionLMServices, IMapper mapper, ILogger<SectionLMController> logger)
		{
			_sectionLMServices = sectionLMServices; _mapper = mapper; _logger = logger;
		}

		#region "Get"

		[Route("SectionLM")]
		public ActionResult SectionLM()
		{
			_logger.LogInformation($"{name}: Выдача данных");

			return View(_sectionLMServices.GetSection());
		}

		#endregion

		#region "Create"

		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания раздела");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SectionLaboratoryMethods LM)
		{
			if (!Validation(LM)) { return View("Create", LM); }

			_logger.LogInformation($"{name}: Создание раздела {LM.id}");

			try
			{
				_sectionLMServices.CreateSection(LM);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		#region "Edit"

		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения раздела {id}");

			var DIM = _sectionLMServices.GetSectionById(id);

			return View(DIM);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(SectionLaboratoryMethods sectionLM)
		{
			if (!Validation(sectionLM)) { return View("Edit", sectionLM); }

			_logger.LogInformation($"{name}: Редактирование раздела {sectionLM.id}");

			try
			{
				_sectionLMServices.EditSection(sectionLM);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		private bool Validation(SectionLaboratoryMethods section)
		{
			if (section is null) { throw new ArgumentNullException("section is null", nameof(section)); }

			if (string.IsNullOrEmpty(section.Name) || section.Name == null)
			{
				ModelState.AddModelError("Invalid sectionDIM", $"*{section.Name}* Название обязательно!");

				return false;
			}

			return true;
		}
	}
}
