﻿using AutoMapper;
using Directory.framework;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Directory.Controllers
{
	///<summary>Контроллер для раздела классификатора лучевой химиотерапии и экстракорпоральных видов</summary>
	public class SectionMRController : JsonController
	{
		private readonly ILogger<SectionMRController> _logger;
		private readonly IMapper _mapper;
		private readonly ISectionMRServices _sectionServices;

		private readonly string name = "Раздел методов медицинских реабилитаций";

		public SectionMRController(ISectionMRServices sectionServices, IMapper mapper, ILogger<SectionMRController> logger)
		{
			_sectionServices = sectionServices; _mapper = mapper; _logger = logger;
		}

		#region "Get"

		[Route("SectionMR")]
		public ActionResult SectionMR()
		{
			_logger.LogInformation($"{name}: Выдача данных");

			return View(_sectionServices.GetSection());
		}

		#endregion

		#region "Create"

		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания раздела");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SectionMedicRehabilitation MR)
		{
			if (!Validation(MR)) { return View("Create", MR); }

			_logger.LogInformation($"{name}: Создание раздела {MR.id}");

			try
			{
				_sectionServices.CreateSection(MR);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		#region "Edit"

		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения раздела {id}");

			var section = _sectionServices.GetSectionById(id);

			return View(section);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(SectionMedicRehabilitation sectionMR)
		{
			if (!Validation(sectionMR)) { return View("Edit", sectionMR); }

			_logger.LogInformation($"{name}: Редактирование раздела {sectionMR.id}");

			try
			{
				_sectionServices.EditSection(sectionMR);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		private bool Validation(SectionMedicRehabilitation section)
		{
			if (section is null) { throw new ArgumentNullException("section is null", nameof(section)); }

			if (string.IsNullOrEmpty(section.Name) || section.Name == null)
			{
				ModelState.AddModelError("Invalid sectionDIM", $"*{section.Name}* Название обязательно!");

				return false;
			}

			return true;
		}
	}
}
