﻿using AutoMapper;
using Directory.framework;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Directory.Controllers
{
	///<summary>Контроллер для раздела классификатора методов медицинской реабилитации</summary>
	public class SectionOPLController : JsonController
	{
		private readonly ILogger<SectionOPLController> _logger;
		private readonly IMapper _mapper;
		private readonly ISectionOPLServices _sectionServices;

		private readonly string name = "Раздел амбулаторно поликлинического уровня";

		public SectionOPLController(ISectionOPLServices sectionServices, IMapper mapper, ILogger<SectionOPLController> logger)
		{
			_sectionServices = sectionServices; _mapper = mapper; _logger = logger;
		}

		#region "Get"

		[Route("SectionOPL")]
		public ActionResult SectionOPL()
		{
			_logger.LogInformation($"{name}: Выдача данных");

			return View(_sectionServices.GetSection());
		}

		#endregion

		#region "Create"

		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания раздела");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SectionOutpatientPolyclinicLevel OPL)
		{
			if (!Validation(OPL)) { return View("Create", OPL); }

			_logger.LogInformation($"{name}: Создание раздела {OPL.id}");

			try
			{
				_sectionServices.CreateSection(OPL);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		#region "Edit"

		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения раздела {id}");

			var section = _sectionServices.GetSectionById(id);

			return View(section);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(SectionOutpatientPolyclinicLevel sectionOPL)
		{
			if (!Validation(sectionOPL)) { return View("Edit", sectionOPL); }

			_logger.LogInformation($"{name}: Редактирование раздела {sectionOPL.id}");

			try
			{
				_sectionServices.EditSection(sectionOPL);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		private bool Validation(SectionOutpatientPolyclinicLevel section)
		{
			if (section is null) { throw new ArgumentNullException("section is null", nameof(section)); }

			if (string.IsNullOrEmpty(section.Name) || section.Name == null)
			{
				ModelState.AddModelError("Invalid sectionDIM", $"*{section.Name}* Название обязательно!");

				return false;
			}

			return true;
		}
	}
}
