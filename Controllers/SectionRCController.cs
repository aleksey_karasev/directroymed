﻿using AutoMapper;
using Directory.framework;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Directory.Controllers
{
	///<summary>Контроллер для раздела классификатора услуг амбулаторно поликлинических уровня и ПМСП</summary>
	public class SectionRCController : JsonController
	{
		private readonly ILogger<SectionRCController> _logger;
		private readonly IMapper _mapper;
		private readonly ISectionRCServices _sectionServices;

		private readonly string name = "Раздел лучевой химеотерапии и экстракорпоральных видов";

		public SectionRCController(ISectionRCServices sectionServices, IMapper mapper, ILogger<SectionRCController> logger)
		{
			_sectionServices = sectionServices; _mapper = mapper; _logger = logger;
		}

		#region "Get"

		[Route("SectionRC")]
		public ActionResult SectionRC()
		{
			_logger.LogInformation($"{name}: Выдача данных");

			return View(_sectionServices.GetSection());
		}

		#endregion

		#region "Create"

		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания раздела");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SectionRayСhemotherapy RC)
		{
			if (!Validation(RC)) { return View("Create", RC); }

			_logger.LogInformation($"{name}: Создание раздела {RC.id}");

			try
			{
				_sectionServices.CreateSection(RC);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		#region "Edit"

		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения раздела {id}");

			var section = _sectionServices.GetSectionById(id);

			return View(section);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(SectionRayСhemotherapy sectionRC)
		{
			if (!Validation(sectionRC)) { return View("Edit", sectionRC); }

			_logger.LogInformation($"{name}: Редактирование раздела {sectionRC.id}");

			try
			{
				_sectionServices.EditSection(sectionRC);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		private bool Validation(SectionRayСhemotherapy section)
		{
			if (section is null) { throw new ArgumentNullException("section is null", nameof(section)); }

			if (string.IsNullOrEmpty(section.Name) || section.Name == null)
			{
				ModelState.AddModelError("Invalid sectionDIM", $"*{section.Name}* Название обязательно!");

				return false;
			}

			return true;
		}
	}
}
