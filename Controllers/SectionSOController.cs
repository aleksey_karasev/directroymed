﻿using AutoMapper;
using Directory.framework;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Directory.Controllers
{
	///<summary>Контроллер для раздела классификатора хирургических операций и манипуляций</summary>
	public class SectionSOController : JsonController
	{
		private readonly ILogger<SectionSOController> _logger;
		private readonly IMapper _mapper;
		private readonly ISectionSOServices _sectionServices;

		private readonly string name = "Раздел хирургических операций и манипуляций";

		public SectionSOController(ISectionSOServices sectionServices, IMapper mapper, ILogger<SectionSOController> logger)
		{
			_sectionServices = sectionServices; _mapper = mapper; _logger = logger;
		}

		#region "Get"

		[Route("SectionSO")]
		public ActionResult SectionSO()
		{
			_logger.LogInformation($"{name}: Выдача данных");

			return View(_sectionServices.GetSection());
		}

		#endregion

		#region "Create"

		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания раздела");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SectionSurgicalOperations SO)
		{
			if (!Validation(SO)) { return View("Create", SO); }

			_logger.LogInformation($"{name}: Создание раздела {SO.id}");

			try
			{
				_sectionServices.CreateSection(SO);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		#region "Edit"

		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения раздела {id}");

			var section = _sectionServices.GetSectionById(id);

			return View(section);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(SectionSurgicalOperations sectionSO)
		{
			if (!Validation(sectionSO)) { return View("Edit", sectionSO); }

			_logger.LogInformation($"{name}: Редактирование раздела {sectionSO.id}");

			try
			{
				_sectionServices.EditSection(sectionSO);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		#endregion

		private bool Validation(SectionSurgicalOperations section)
		{
			if (section is null) { throw new ArgumentNullException("section is null", nameof(section)); }

			if (string.IsNullOrEmpty(section.Name) || section.Name == null)
			{
				ModelState.AddModelError("Invalid sectionDIM", $"*{section.Name}* Название обязательно!");

				return false;
			}

			return true;
		}
	}
}
