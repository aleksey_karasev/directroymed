﻿using AutoMapper;
using ClosedXML.Excel;
using Directory.framework;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace Directory.Controllers
{
	///<summary>Контроллер для классификатора хирургических операций и манипуляций</summary>
	public class SurgicalOperationsController : JsonController
	{
		private readonly ILogger<SurgicalOperationsController> _logger;
		private readonly IMapper _mapper;
		private readonly ISurgicalOperationsServices _services;
		private readonly ISectionSOServices _sectionServices;
		private readonly IViewStudyServices _viewStudyServices;

		private readonly string name = "Справочник хирургических операций и манипуляций";

		public SurgicalOperationsController(
			ILogger<SurgicalOperationsController> logger,
			ISurgicalOperationsServices services,
			IMapper mapper,
			ISectionSOServices sectionServices,
			IViewStudyServices viewStudyServices)
		{
			_logger = logger;
			_services = services;
			_mapper = mapper;
			_sectionServices = sectionServices;
			_viewStudyServices = viewStudyServices;
		}

		[Route("SurgicalOperations")]
		public ActionResult SurgicalOperations(SurgicalOperationsFilter filter)
		{
			if (filter is null) { throw new ArgumentNullException("filter is null", nameof(filter)); }

			_logger.LogInformation($"{name}: Выдача данных");

			try
			{
				var model = _services.FilterSurgicalOperations(filter);

				filter.SOMethods = model;

				return View(filter);
			}
			//При ошибки может быть дисконект с сервером и тд
			catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return View("BadRequest"); }
			catch (Exception e) { return StatusCode(500, e.Message); }
		}


		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания справочника");

			ViewBag.SectionList = _sectionServices.GetSectionWithCreate();
			ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SOViewModel model)
		{
			if (!Validation(model)) { return View("Create", model); }

			_logger.LogInformation($"{name}: Создание справочника {model.Id}");

			SurgicalOperations SO = _mapper.Map<SurgicalOperations>(model);

			try
			{
				_services.CreateSO(SO);

				return RedirectToAction(nameof(Index));
			}
			catch (ArgumentOutOfRangeException e) { ViewBag.BadRequestMessage = e.Message; return View("BadRequest"); }
			catch (Exception e) { return StatusCode(500, e.Message); }
		}



		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения справочника {id}");

			ViewBag.SectionList = _sectionServices.GetSectionWithCreate();
			ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

			return View(_services.GetSOById(id));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int id, SOViewModel model)
		{
			if (!Validation(model)) { return View("Edit", model); }

			_logger.LogInformation($"{name}: Редактирование справочника {model.Id}");

			SurgicalOperations SO = _mapper.Map<SurgicalOperations>(model);

			try
			{

				_services.EditSO(SO);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}



		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id)
		{
			try
			{
				_logger.LogInformation($"{name}: Удаление справочника {id}");

				_services.DeleteSO(id);
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		[HttpGet]
		[Route("SurgicalOperations/API")]
		public ActionResult API() { return Ok("{Сайт} /api/Token/GetSOByToken?token= {Ваш TOKEN}"); }


		[HttpGet]
		[Route("SurgicalOperations/Excel")]
		public IActionResult Excel()
		{
			var datas = _services.FilterSurgicalOperations(null).OrderBy(e => e.SectionCode).OrderBy(e => e.CodeStudy);

			var workbook = new XLWorkbook();
			var worksheet = workbook.Worksheets.Add("directory");
			var currentRow = 1;
			worksheet.Cell(currentRow, 1).Value = "Вид исследования";
			worksheet.Cell(currentRow, 2).Value = "Код раздела";
			worksheet.Cell(currentRow, 3).Value = "Название раздела";
			worksheet.Cell(currentRow, 4).Value = "Порядковый номер";
			worksheet.Cell(currentRow, 5).Value = "Код исследования";
			worksheet.Cell(currentRow, 6).Value = "Название исследования";

			foreach (var data in datas)
			{
				currentRow++;
				worksheet.Cell(currentRow, 1).Value = data.ViewStudy;
				worksheet.Cell(currentRow, 2).Value = data.SectionCode;
				worksheet.Cell(currentRow, 3).Value = data.SectionName;
				worksheet.Cell(currentRow, 4).Value = data.OrdinalNumber;
				worksheet.Cell(currentRow, 5).Value = data.CodeStudy;
				worksheet.Cell(currentRow, 6).Value = data.NameStude;
			}

			var stream = new MemoryStream();
			workbook.SaveAs(stream);
			var content = stream.ToArray();

			return File(
				content,
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				"SurgicalOperations.xlsx");
		}

		private bool Validation(SOViewModel model)
		{
			if (model is null) { throw new ArgumentNullException("model is null", nameof(model)); }

			if (string.IsNullOrEmpty(model.SectionCode) || model.SectionCode == null)
			{
				ModelState.AddModelError("Invalid SectionName", $"*{model.SectionCode}* Раздел обязателен!");

				ViewBag.SectionList = _sectionServices.GetSectionWithCreate();

				ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

				return false;
			}
			else if (string.IsNullOrEmpty(model.NameStude) || model.NameStude == null)
			{
				ModelState.AddModelError("Invalid NameStude", $"*{model.NameStude}* Название исследования обязательно!");

				ViewBag.SectionList = _sectionServices.GetSectionWithCreate();

				ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

				return false;
			}
			else if (string.IsNullOrEmpty(model.ViewStudy) || model.ViewStudy == null)
			{
				ModelState.AddModelError("Invalid ViewStudy", $"*{model.ViewStudy}* Вид исследования обязательно!");

				ViewBag.SectionList = _sectionServices.GetSectionWithCreate();

				ViewBag.ViewStudyList = _viewStudyServices.GetViewStudyWithCreate();

				return false;
			}

			return true;
		}
	}
}
