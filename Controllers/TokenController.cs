﻿using Directory.framework;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Directory.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class TokenController : JsonController
	{
		private readonly ILogger<TokenController> _logger;
		private readonly ITokenServices _tokenServices;

		private readonly IDiagnInstrumMethodsServices _diagnInstrumMethodsServices;
		private readonly ILaboratoryMethodsServices _laboratoryMethodsServices;
		private readonly IMedicRehabilitationServices _medicRehabilitationServices;
		private readonly IOutpatientPolyclinicLevelServices _outpatientPolyclinicLevelServices;
		private readonly IRayСhemotherapyServices _rayСhemotherapyServices;
		private readonly ISurgicalOperationsServices _surgicalOperationsServices;

		private readonly ISectionDIMServices _sectionDIMServices;
		private readonly ISectionLMServices _sectionLMServices;
		private readonly ISectionMRServices _sectionMRServices;
		private readonly ISectionOPLServices _sectionOPLServices;
		private readonly ISectionRCServices _sectionRCServices;
		private readonly ISectionSOServices _sectionSOServices;

		private readonly IPersonServices _personServices;
		private readonly IViewStudyServices _viewStudyServices;

		public TokenController(
			ILogger<TokenController> logger,
			ITokenServices tokenServices,

			IDiagnInstrumMethodsServices diagnInstrumMethodsServices,
			ILaboratoryMethodsServices laboratoryMethodsServices,
			IMedicRehabilitationServices medicRehabilitationServices,
			IOutpatientPolyclinicLevelServices outpatientPolyclinicLevelServices,
			IRayСhemotherapyServices rayСhemotherapyServices,
			ISurgicalOperationsServices surgicalOperationsServices,

			ISectionDIMServices sectionDIMServices,
			ISectionLMServices sectionLMServices,
			ISectionMRServices sectionMRServices,
			ISectionOPLServices sectionOPLServices,
			ISectionRCServices sectionRCServices,
			ISectionSOServices sectionSOServices,

			IPersonServices personServices,
			IViewStudyServices viewStudyServices)
		{
			_logger = logger;
			_tokenServices = tokenServices;

			_diagnInstrumMethodsServices = diagnInstrumMethodsServices;
			_laboratoryMethodsServices = laboratoryMethodsServices;
			_medicRehabilitationServices = medicRehabilitationServices;
			_outpatientPolyclinicLevelServices = outpatientPolyclinicLevelServices;
			_rayСhemotherapyServices = rayСhemotherapyServices;
			_surgicalOperationsServices = surgicalOperationsServices;

			_sectionDIMServices = sectionDIMServices;
			_sectionLMServices = sectionLMServices;
			_sectionMRServices = sectionMRServices;
			_sectionOPLServices = sectionOPLServices;
			_sectionRCServices = sectionRCServices;
			_sectionSOServices = sectionSOServices;

			_personServices = personServices;
			_viewStudyServices = viewStudyServices;
		}

		#region "Справочники"

		[HttpGet]
		[Route("GetDIMByToken")]
		public IActionResult GetDIMByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Справочник диагностических интрсументальных методов: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _diagnInstrumMethodsServices.FilterDiagnInstrumMethods(null);

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		[HttpGet]
		[Route("GetLMByToken")]
		public IActionResult GetLMByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Справочник лабораторных методов исследования: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _laboratoryMethodsServices.FilterLaboratoryMethods(null);

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		[HttpGet]
		[Route("GetMRByToken")]
		public IActionResult GetMRByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Справочник методов медицинских реабилитаций: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _medicRehabilitationServices.FilterMedicRehabilitation(null);

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}


		[HttpGet]
		[Route("GetOPLByToken")]
		public IActionResult GetOPLByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Справочник амбулаторно поликлинического уровня: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _outpatientPolyclinicLevelServices.FilterOutpatientPolyclinicLevel(null);

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}


		[HttpGet]
		[Route("GetRCByToken")]
		public IActionResult GetRCByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Справочник лучевой химеотерапии и экстракорпоральных видов: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _rayСhemotherapyServices.FilterRayСhemotherapy(null);

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}


		[HttpGet]
		[Route("GetSOByToken")]
		public IActionResult GetSOByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Справочник хирургических операций и манипуляций: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _surgicalOperationsServices.FilterSurgicalOperations(null);

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		#endregion

		#region "Разделы"

		[HttpGet]
		[Route("SectionDIMByToken")]
		public IActionResult SectionDIMByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Раздел диагностических интрсументальных методов: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _sectionDIMServices.GetSection();

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		[HttpGet]
		[Route("SectionLMByToken")]
		public IActionResult SectionLMByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Раздел лабораторных методов исследования: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _sectionLMServices.GetSection();

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		[HttpGet]
		[Route("SectionMRByToken")]
		public IActionResult SectionMRByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Раздел методов медицинских реабилитаций: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _sectionMRServices.GetSection();

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}


		[HttpGet]
		[Route("SectionOPLByToken")]
		public IActionResult SectionOPLByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Раздел амбулаторно поликлинического уровня: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _sectionOPLServices.GetSection();

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		[HttpGet]
		[Route("SectionRCByToken")]
		public IActionResult SectionRCByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Раздел лучевой химеотерапии и экстракорпоральных видов: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _sectionRCServices.GetSection();

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		[HttpGet]
		[Route("SectionSOByToken")]
		public IActionResult SectionSOByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Раздел хирургических операций и манипуляций: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _sectionSOServices.GetSection();

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		#endregion

		#region "Вид исследования"

		[HttpGet]
		[Route("ViewStudyByToken")]
		public IActionResult ViewStudyByToken(string token)
		{
			try
			{
				var tokens = _tokenServices.GetTokens().FirstOrDefault(e => e.Tokens == token);

				if (tokens == null) { return Json("Ошибка! Данного токена не было найдено"); }

				_logger.LogInformation($"Вид исследований: Выдача данных ПО ТОКЕНУ пользователя: {tokens.Person}");

				if (tokens.DateEnd != new DateTime()) { return BadRequest("Вы заблокированы"); }
				else
				{
					var dir = _viewStudyServices.GetViewStudy();

					if (dir.Count == 0) { return Json("Отсутствуют данные"); }

					return Json(dir);
				}
			}
			catch { return Json("Ошибка! Данного токена не было найдено"); }
		}

		#endregion
	}
}
