﻿using Directory.framework;
using Directory.Models.Directoryes.Additionally;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Controllers
{
	public class ViewStudyController : JsonController
	{
		private readonly ILogger<ViewStudyController> _logger;
		private readonly IViewStudyServices _viewStudyServices;

		public ViewStudyController(IViewStudyServices viewStudyServices, ILogger<ViewStudyController> logger) { _viewStudyServices = viewStudyServices; _logger = logger; }

		private readonly string name = "Виды исследования";

		[Route("ViewStudy")]
		public ActionResult ViewStudy()
		{
			_logger.LogInformation($"{name}: Выдача данных");

			return View(_viewStudyServices.GetViewStudy());
		}


		public ActionResult Create()
		{
			_logger.LogInformation($"{name}: Попытка создания вида");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(ViewStudy model)
		{

			if (string.IsNullOrEmpty(model.Name))
			{
				ModelState.AddModelError("Invalid Name", $"*{model.Name}* Раздел обязателен!");

				return View("Create", model);
			}

			if (_viewStudyServices.GetViewStudy().Where(e => e.Name == model.Name).Count() > 0)
			{
				ModelState.AddModelError("Invalid Name", $"Данный вид исследования уже существует");

				return View("Create", model);
			}

			_logger.LogInformation($"{name}: Создание вида {model.id}");

			try
			{
				_viewStudyServices.CreateViewStudy(model);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: ViewStudyController/Edit/5
		public ActionResult Edit(int id)
		{
			_logger.LogInformation($"{name}: Попытка изменения вида {id}");

			var VS = _viewStudyServices.GetViewStudyById(id);

			return View(VS);
		}

		// POST: ViewStudyController/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(ViewStudy VS)
		{
			if (string.IsNullOrEmpty(VS.Name))
			{
				ModelState.AddModelError("Invalid Name", $"*{VS.Name}* Раздел обязателен!");

				return View("Create", VS);
			}

			if (_viewStudyServices.GetViewStudy().Where(e => e.Name == VS.Name).Count() > 0)
			{
				ModelState.AddModelError("Invalid Name", $"Данный вид исследования уже существует");

				return View("Create", VS);
			}

			_logger.LogInformation($"{name}: Редактирование вида {VS.id}");

			try
			{
				_viewStudyServices.EditViewStudy(VS);

				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
	}
}
