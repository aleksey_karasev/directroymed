﻿using Directory.Data.Common;
using Directory.Data.Seeds;
using Directory.Models;
using Directory.Models.Directoryes;
using Directory.Models.Directoryes.Additionally;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Identity;
using Directory.ModelsConfiguraion;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Directory.Models.ModelsView;
using Directory.Tokens;

namespace Directory.Data
{
	public class ApplicationDbContext : IdentityDbContext<Person, Role, string,
	IdentityUserClaim<string>, PersonRole, IdentityUserLogin<string>, IdentityRoleClaim<string>, IdentityUserToken<string>>
	{
		private readonly IEntityConfigurationContainer _entityContainer;

		//Как назвали базы здесь так и в бд будут
		public DbSet<Person> Persons { get; set; }


		///<summary>Медицинские справочники (DirectoryesMedic)</summary>
		public DbSet<DiagnInstrumMethods> DiagnInstrumMethods { get; set; }
		public DbSet<LaboratoryMethods> LaboratoryMethods { get; set; }
		public DbSet<RayСhemotherapy> RayСhemotherapy { get; set; }
		public DbSet<MedicRehabilitation> MedicRehabilitation { get; set; }
		public DbSet<OutpatientPolyclinicLevel> OutpatientPolyclinicLevel { get; set; }
		public DbSet<SurgicalOperations> SurgicalOperations { get; set; }


		///<summary>Cправочники разделов (Directoryes)</summary>
		public DbSet<SectionDiagnInstrum> SectionDiagnInstrum { get; set; }
		public DbSet<SectionLaboratoryMethods> SectionLaboratoryMethods { get; set; }
		public DbSet<SectionRayСhemotherapy> SectionRayСhemotherapy { get; set; }
		public DbSet<SectionMedicRehabilitation> SectionMedicRehabilitation { get; set; }
		public DbSet<SectionOutpatientPolyclinicLevel> SectionOutpatientPolyclinicLevel { get; set; }
		public DbSet<SectionSurgicalOperations> SectionSurgicalOperations { get; set; }


		///<summary>Вид исследования справочник</summary>
		public DbSet<ViewStudy> ViewStudy { get; set; }

		public DbSet<Token> Token { get;set;}


		public ApplicationDbContext(DbContextOptions options, IEntityConfigurationContainer entityContainer)
		: base(options)
		{
			_entityContainer = entityContainer;
		}

		public ApplicationDbContext() { } 

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.ApplyConfiguration(new PersonRoleConfiguration());

			builder.PersonSeedData();
			builder.RoleSeedData();
			builder.PersonRoleSeedData();

			builder.AddDIMSeepdsData();
			builder.AddLMSeepdsData();
			builder.AddMRSeepdsData();
			builder.AddOPLSeepdsData();
			builder.AddRCSeepdsData();
			builder.AddSOSeepdsData();

			builder.AddSectionDIMSeepdsData();
			builder.AddSectionLMSeepdsData();
			builder.AddSectionMRSeepdsData();
			builder.AddSectionOPLSeepdsData();
			builder.AddSectionRCSeepdsData();
			builder.AddSectionSOSeepdsData();

			builder.AddViewStudySeedsData();

			builder.Entity(_entityContainer.DiagnInstrumMethodsConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.LaboratoryMethodsConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.RayСhemotherapyConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.MedicRehabilitationConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.OutpatientPolyclinicLevelConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.SurgicalOperationsConfiguration.ProvideConfigurationAction());

			builder.Entity(_entityContainer.SectionDIMConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.SectionLMConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.SectionMRConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.SectionOPLConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.SectionRCConfiguration.ProvideConfigurationAction());
			builder.Entity(_entityContainer.SectionSOConfiguration.ProvideConfigurationAction());

			builder.Entity(_entityContainer.ViewStudyConfiguration.ProvideConfigurationAction());

			builder.Entity(_entityContainer.TokenConfiguration.ProvideConfigurationAction());
		}
	}
}
