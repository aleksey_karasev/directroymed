﻿using Directory.Data;
using Directory.ModelsConfiguraion;
using Microsoft.EntityFrameworkCore;
using System;

namespace Directory.Data
{
	///<summary>Выполняет унаследование от себя функцию Create, для создания фабрики</summary>
	public interface IApplicationDbContextFactory { ApplicationDbContext Create(); }

	///<summary>Выполняет конвеерное создание класса <paramref name="AppDbContext"/>, для облегченного использования</summary>
	public class ApplicationDbContextFactory : IApplicationDbContextFactory
	{
		private readonly DbContextOptions _options; //Настройка соединения с БД
		private readonly IEntityConfigurationContainer _entityContainer;

		///<summary>Выполняет создание фабрики соединения с БД</summary>
		public ApplicationDbContextFactory(DbContextOptions options, IEntityConfigurationContainer entityContainer)
		{
			if(options is null || entityContainer is null) { throw new ArgumentNullException("Настройка соединения пуста"); }
			_options = options;
			_entityContainer = entityContainer;
		}

		///<summary>Выполняет создания соединения с текущими настройками</summary>
		public ApplicationDbContext Create() { return new ApplicationDbContext(_options, _entityContainer); }
	}
}
