﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Data.Common
{
	public static class CustomClaimTypes
	{
		public const string FullName = "FullName";
		public const string DirectoryName = "DirectoryName";
		public const string DirectoryId = "DirectoryId";
	}
}
