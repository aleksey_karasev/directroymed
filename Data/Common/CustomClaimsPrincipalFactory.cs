﻿using Directory.Models;
using Directory.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Data.Common
{
	public class CustomClaimsPrincipalFactory : UserClaimsPrincipalFactory<Person, Role>
	{
        private readonly ApplicationDbContext _applicationDbContext;
        public CustomClaimsPrincipalFactory(UserManager<Person> userManager, RoleManager<Role> roleManager,
           IOptions<IdentityOptions> optionsAccessor, ApplicationDbContext applicationDbContext) : base(userManager, roleManager, optionsAccessor)
        {
            _applicationDbContext = applicationDbContext;
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(Person person)
        {
            //DirectoryMedic directoryName = _applicationDbContext.DirectoryesMedic.FirstOrDefault(p => p.id == person.DirectoryId);
            
            var identity = await base.GenerateClaimsAsync(person);
            identity.AddClaim(new Claim(CustomClaimTypes.FullName, $"{person.LastName} {person.FirstName} {person.MiddleName}"));
            //identity.AddClaim(new Claim(CustomClaimTypes.DirectoryId, person.DirectoryId.ToString()));
            //identity.AddClaim(new Claim(CustomClaimTypes.DirectoryName, directoryName.Name));

            _applicationDbContext.Dispose(); //После получения данных из бд, отключаемся от нее сразу

            return identity;
        }
    }
}
