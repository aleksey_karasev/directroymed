﻿using Directory.Models.Identity;
using Directory.ModelsConfiguraion;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Data.Common
{
	public class PersonRoleConfiguration : IEntityTypeConfiguration<PersonRole>
	{
        public void Configure(EntityTypeBuilder<PersonRole> builder)
        {
            builder.HasKey(k => new { k.UserId, k.RoleId });

            builder.HasOne(c => c.Role).WithMany(m => m.PersonRoles).HasForeignKey(b => b.RoleId);
            builder.HasOne(c => c.User).WithMany(m => m.PersonRole).HasForeignKey(b => b.UserId);
        }
	}
}
