﻿using CsvHelper;
using Directory.Models.DirectoryesMedic;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;

namespace Directory.Data.Seeds
{
	internal static class LMSeepds
	{
        internal static ModelBuilder AddLMSeepdsData(this ModelBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Directory.Data.CsvFiles.LM.csv";
            Stream stream = assembly.GetManifestResourceStream(resourceName);
            StreamReader reader = new(stream, Encoding.UTF8);
            CsvReader csvReader = new(reader, CultureInfo.CurrentCulture);
            csvReader.Read();
            csvReader.ReadHeader();

            int index = 1;
            while (csvReader.Read())
            {
                var DIM = new LaboratoryMethods()
                {
                    SectionId = int.Parse(csvReader.GetField("Код раздела исследования")),
                    id = index++,
                    ViewStudy = csvReader.GetField("Вид исследований"),
                    SectionCode = csvReader.GetField("Код раздела исследования"),
                    SectionName = csvReader.GetField("Раздел"),
                    OrdinalNumber = csvReader.GetField("Порядковый номер"),
                    CodeStudy = csvReader.GetField("Код исследования"),
                    NameStude = csvReader.GetField("Наименование исследования")
                };

                builder.Entity<LaboratoryMethods>().HasData(DIM);
            }

            return builder;
        }
    }
}
