﻿using Directory.Models;
using Directory.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Data.Seeds
{
	internal static class PersonRoleSeed
	{
		internal static ModelBuilder PersonSeedData(this ModelBuilder builder)
		{
			const string password = "med&dir1";
			var users = new Person[]
			{
				new Person
				{
					Id = "5e5d1198-3ab6-4a52-9a48-8ac816eabb00",
					LastName = "Администратор",
					FirstName = "Администратор",
					LockoutEnabled = false,
					UserName = "administrator",
					Email = "admin@mail.kg",
					NormalizedEmail = "admin@mail.kg".ToUpper(),
					NormalizedUserName = "administrator".ToUpper(),
					SecurityStamp = "IHUMVMO2CKKJ2P3JKIZGD27EBBXKQDCR",
					ConcurrencyStamp = "0ea7587d-40dc-44b7-8833-f1a94c223e4e",
				},

				new Person
				{
					Id = "0f272801-106d-480e-8faa-23cb4bcac758",
					LastName = "Пользователь",
					FirstName = "Пользователь",
					LockoutEnabled = false,
					UserName = "user",
					Email = "user@mail.kg",
					NormalizedEmail = "user@mail.kg".ToUpper(),
					NormalizedUserName = "user".ToUpper(),
					SecurityStamp = "L3U7OOGNLMXLZ6SYS2YVATY2RRRNO2AR",
					ConcurrencyStamp = "4ec7c2a5-50ce-435f-9c51-4c02fe747287"
				}
			};

			//Заполняет пароли изначально используемых пользователей
			foreach (var user in users)
			{
				user.PasswordHash = new PasswordHasher<Person>().HashPassword(user, password);
			}

			builder.Entity<Person>().HasData(users);

			return builder;
		}

		internal static ModelBuilder RoleSeedData(this ModelBuilder builder)
		{
			var roles = new Role[]
			{
				new Role
				{
					Id = "1b96eede-75fd-4090-a5bc-d70b0947b861",
					ConcurrencyStamp = "9b66a6dd-61e7-4f40-b248-d61b19364772",
					Name = "Администратор",
					NormalizedName = "Администратор".ToUpper()
				},

				new Role
				{
					Id = "64212991-132e-4c78-ab87-a5b11015f60a",
					ConcurrencyStamp = "2e92c936-767b-4f42-8b2b-29c286cb78da",
					Name = "Пользователь",
					NormalizedName = "Пользователь".ToUpper()
				}
			};

			builder.Entity<Role>().HasData(roles);

			return builder;
		}

		internal static ModelBuilder PersonRoleSeedData(this ModelBuilder builder)
		{
			var userRoles = new PersonRole[]
			{
				new PersonRole
				{
					UserId = "5e5d1198-3ab6-4a52-9a48-8ac816eabb00",  // Id admin@mail.kg
                    RoleId = "1b96eede-75fd-4090-a5bc-d70b0947b861"   // Id Администратор
                },

				new PersonRole
				{
					UserId = "0f272801-106d-480e-8faa-23cb4bcac758",  // Id user@mail.kg
                    RoleId = "64212991-132e-4c78-ab87-a5b11015f60a"   // Id Пользователь
                }
			};

			builder.Entity<PersonRole>().HasData(userRoles);

			return builder;
		}
	}
}
