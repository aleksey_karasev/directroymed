﻿using CsvHelper;
using Directory.Models.DirectoryesMedic;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;

namespace Directory.Data.Seeds
{
	internal static class RCSeepds
	{
		internal static ModelBuilder AddRCSeepdsData(this ModelBuilder builder)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			string resourceName = "Directory.Data.CsvFiles.RC.csv";
			Stream stream = assembly.GetManifestResourceStream(resourceName);
			StreamReader reader = new(stream, Encoding.UTF8);
			CsvReader csvReader = new(reader, CultureInfo.CurrentCulture);
			csvReader.Read();
			csvReader.ReadHeader();

			int index = 1;
			while (csvReader.Read())
			{
				var DIM = new RayСhemotherapy()
				{
					SectionId = int.Parse(csvReader.GetField("Код раздела")),
					id = index++,
					ViewStudy = csvReader.GetField("Вид"),
					SectionCode = csvReader.GetField("Код раздела"),
					SectionName = csvReader.GetField("Раздел"),
					OrdinalNumber = csvReader.GetField("Порядковый номер"),
					CodeStudy = csvReader.GetField("Код"),
					NameStude = csvReader.GetField("Наименование")
				};

				builder.Entity<RayСhemotherapy>().HasData(DIM);
			}

			return builder;
		}
	}
}
