﻿using CsvHelper;
using Directory.Models.Directoryes;
using Directory.Models.DirectoryesMedic;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;

namespace Directory.Data.Seeds
{
	internal static class SectionSOSeepds
	{
		internal static ModelBuilder AddSectionSOSeepdsData(this ModelBuilder builder)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			string resourceName = "Directory.Data.CsvFiles.SO.csv";
			Stream stream = assembly.GetManifestResourceStream(resourceName);
			StreamReader reader = new(stream, Encoding.UTF8);
			CsvReader csvReader = new(reader, CultureInfo.CurrentCulture);
			csvReader.Read();
			csvReader.ReadHeader();

			List<string> backNameSection = new List<string>();
			int index = 1;
			while (csvReader.Read())
			{
				if (backNameSection.Contains(csvReader.GetField("Раздел"))) { continue; }

				var DIM = new SectionSurgicalOperations()
				{
					PersonId = null,
					id = index++,
					LockedSection = false,
					Name = csvReader.GetField("Раздел"),
				};

				backNameSection.Add(DIM.Name);

				builder.Entity<SectionSurgicalOperations>().HasData(DIM);
			}

			return builder;
		}
	}
}
