﻿using CsvHelper;
using Directory.Models.Directoryes.Additionally;
using Directory.Models.DirectoryesMedic;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;

namespace Directory.Data.Seeds
{
	internal static class ViewStudySeeds
	{
		internal static ModelBuilder AddViewStudySeedsData(this ModelBuilder builder)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();

			string[] resourceName = new string[] {
			"Directory.Data.CsvFiles.DIM.csv",
			"Directory.Data.CsvFiles.LM.csv",
			"Directory.Data.CsvFiles.MR.csv",
			"Directory.Data.CsvFiles.OPL.csv",
			"Directory.Data.CsvFiles.RC.csv",
			"Directory.Data.CsvFiles.SO.csv" };

			int index = 1;
			List<string> backNameView = new List<string>();

			foreach (var resource in resourceName)
			{

				Stream stream = assembly.GetManifestResourceStream(resource);
				StreamReader reader = new(stream, Encoding.UTF8);
				CsvReader csvReader = new(reader, CultureInfo.CurrentCulture);
				csvReader.Read();
				csvReader.ReadHeader();

				string view = "Вид";

				while (csvReader.Read())
				{

					//В Excel вид записали по разному, приходится искать разные поля
				error:
					try
					{
						if (backNameView.Contains(csvReader.GetField(view))) { continue; }
					}
					catch { if (view == "Вид") { view = "Вид исследований"; } else { view = "Вид"; } goto error; }

					var VS = new ViewStudy()
					{
						id = index++,
						LockedSection = false,
						PersonId = null,
						Name = csvReader.GetField(view)
					};

					backNameView.Add(VS.Name);

					builder.Entity<ViewStudy>().HasData(VS);
				}
			}

			return builder;
		}
	}
}
