﻿using Microsoft.Extensions.Configuration;
using Directory.Data;
using Directory.Data.Common;
using Directory.Models;
using Directory.Models.Identity;
using Directory.ModelsConfiguraion;
using Directory.Services;
using Directory.Services.interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Helper;
using System.Reflection;

namespace Directory
{
	public static class DependencyInjection
	{
		public static IServiceCollection AddDirectoryServices(this IServiceCollection services, IConfiguration configuration)
		{
			DbContextOptionsBuilder<ApplicationDbContext> optionsDb = new();  //Настройки подключения

			optionsDb.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));

			//Создаем это подключение единожды, для многоразового пользования
			services.AddSingleton<IApplicationDbContextFactory>(sp => new ApplicationDbContextFactory(optionsDb.Options, new EntityConfigurationContainer()));

			services.AddScoped<IEntityConfigurationContainer>(sp => new EntityConfigurationContainer());

			services.AddScoped<ICurrentPersonServices, CurrentPersonServices>();

			services.AddScoped<IPersonServices, PersonServices>();

			services.AddScoped<IViewStudyServices, ViewStudyServices>();

			services.AddScoped<ITokenServices, TokenServices>();


			services.AddScoped<IDiagnInstrumMethodsServices, DiagnInstrumMethodsServices>();

			services.AddScoped<ILaboratoryMethodsServices, LaboratoryMethodsServices>();

			services.AddScoped<IMedicRehabilitationServices, MedicRehabilitationServices>();

			services.AddScoped<IOutpatientPolyclinicLevelServices, OutpatientPolyclinicLevelServices>();

			services.AddScoped<IRayСhemotherapyServices, RayСhemotherapyServices>();

			services.AddScoped<ISurgicalOperationsServices, SurgicalOperationsServices>();


			services.AddScoped<ISectionDIMServices, SectionDIMServices>();

			services.AddScoped<ISectionLMServices, SectionLMServices>();

			services.AddScoped<ISectionMRServices, SectionMRServices>();

			services.AddScoped<ISectionOPLServices, SectionOPLServices>();

			services.AddScoped<ISectionRCServices, SectionRCServices>();

			services.AddScoped<ISectionSOServices, SectionSOServices>();


			services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();

			return services;
		}

		public static IServiceCollection AddApplicationDbContext(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<ApplicationDbContext>(options =>
				options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));

			Type t = typeof(MappingProfile);
			Assembly assembly = t.Assembly;
			services.AddAutoMapper(assembly);

			services.AddIdentity<Person, Role>(options =>
			{
				options.Password.RequiredLength = 8;                // минимальная длина
				options.Password.RequireNonAlphanumeric = false;    // требуются ли не алфавитно-цифровые символы
				options.Password.RequireLowercase = false;          // требуются ли символы в нижнем регистре
				options.Password.RequireUppercase = false;          // требуются ли символы в верхнем регистре
				options.Password.RequireDigit = true;               // требуются ли цифры

				//Блокировка при 10 неверных попытках на 10 минут
				options.Lockout.MaxFailedAccessAttempts = 10;
				options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);

				options.User.RequireUniqueEmail = false;    // уникальный email

			})
				.AddEntityFrameworkStores<ApplicationDbContext>()
				.AddClaimsPrincipalFactory<CustomClaimsPrincipalFactory>();
			//.AddTokenProvider();


			return services;
		}
	}
}
