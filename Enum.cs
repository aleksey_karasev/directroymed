﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory
{
	public enum Access
	{
		Null = 0,
		Open = 1,
		Close = 2
	}
}
