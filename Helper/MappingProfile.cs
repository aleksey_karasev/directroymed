﻿
using AutoMapper;
using Directory.Models;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Identity;
using Directory.Models.ModelsView;
using Directory.Models.ModelsView.DirectoryesMedic;
using System.Collections.Generic;

namespace Directory.Helper
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateProgrammingToPerson();
		}

		private void CreateProgrammingToPerson()
		{
			CreateMap<Person, PersonView>();

			CreateMap<PersonView, Person>();

			CreateMap<DiagnInstrumMethods, DIMViewModel>();
			//.ForMember(to => to.SectionName, from => from.MapFrom(bas => bas.Section != null ? bas.Section.Name  : "No name"));

			CreateMap<DIMViewModel, DiagnInstrumMethods>();
					//.ForMember(to => to.Section, from => from.MapFrom(bas => bas.SectionName != null ? bas.SectionName : "No name"));

			CreateMap<LaboratoryMethods, LMViewModel>();

			CreateMap<LMViewModel, LaboratoryMethods>();

			CreateMap<MedicRehabilitation, MRViewModel>();

			CreateMap<MRViewModel, MedicRehabilitation>();

			CreateMap<OutpatientPolyclinicLevel, OPLViewModel>();

			CreateMap<OPLViewModel, OutpatientPolyclinicLevel>();

			CreateMap<RayСhemotherapy, RCViewModel>();

			CreateMap<RCViewModel, RayСhemotherapy>();

			CreateMap<SurgicalOperations, SOViewModel>();

			CreateMap<SOViewModel, SurgicalOperations>();

			//Пример применения маппера
			//CreateMap<Programming, ProgrammingPersView>()
			//	.ForMember(to => to.ProgrammingName, from => from.MapFrom(bas => bas.name == null || bas.name.Trim() == "" ? "Not Programming" : bas.name));

			//CreateMap<Person, ProgrammingPersView>()
			//.ForMember(to => to.PersonsList, from => from.MapFrom(bas => bas.firstName == null || bas.firstName.Trim() == "" ? "No Name" : bas.firstName));
		}
	}
}
