﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Directory.Mails
{
	///<summary>Описывает изменяемый адрес электронной почты</summary>
	public class Mail
	{
		#region " Локальные поля "

		string _mail;
		string _address;
		string _domain;
		string _zone;

		#endregion

		#region " Конструкторы "

		///<summary>Создает объект <see cref="Mail"/></summary>
		///<param name="mail">Адрес электронной почты</param>
		public Mail(string mail)
		{
			var components = parseMail(mail);
			Address = components.address;
			Domain = components.domain;
			Zone = components.zone;
			MailString = mail;
		}

		///<summary>Создает объект <see cref="Mail"/></summary>
		///<param name="address">Имя электронной почты</param>
		///<param name="host">Хост электронной почты</param>
		public Mail(string address, string host)
		{
			var components = parseMail(address, host);
			Address = components.address;
			Domain = components.domain;
			Zone = components.zone;
		}

		///<summary>Создает объект <see cref="Mail"/></summary>
		///<param name="address">Имя электронной почты</param>
		///<param name="domain">Домен электронной почты</param>
		///<param name="zone">Зона электронной почты</param>
		public Mail(string address, string domain, string zone) { Address = address; Domain = domain; Zone = zone; }
		#endregion

		#region " Свойства "

		///<summary>Возвращает и устанавливает адрес электронной почты</summary>
		public string Address
		{
			get => _address;
			set
			{
				if (validation(value) != null) { throw new ArgumentException("В адресе электронной почты допущена ошибка", nameof(value)); }
				_address = normalization(value);
			}
		}

		///<summary>Возвращает и устанавливает домен электронной почты</summary>
		public string Domain
		{
			get => _domain;
			set
			{
				if (validation(value) != null) { throw new ArgumentException("В домене электронной почты допущена ошибка", nameof(value)); }
				_domain = normalization(value);
			}
		}

		///<summary>Возвращает и устанавливает зону электронной почты</summary>
		public string Zone
		{
			get => _zone;
			set
			{
				if (validation(value, true) != null) { throw new ArgumentException("В зоне электронной почты допущена ошибка", nameof(value)); }
				_zone = normalization(value);
			}
		}

		///<summary>Возвращает хост электронной почты</summary>
		public string Host => $"{_domain}.{_zone}";

		///<summary>Возвращает хост электронной почты</summary>
		public string MailString
		{
			get => _mail;
			set { _mail = value; }
		}


		#endregion

		#region " Функции | Методы "

		///<summary>Парсинг электронной почты</summary>
		///<param name="mail">Адрес электронной почты</param>
		private (string address, string domain, string zone) parseMail(string mail)
		{
			mail = mail.Trim();
			int at = mail.IndexOf('@'), dot = mail.LastIndexOf('.');
			if (mail.Length < 5 || at < 1 || dot < 3 || dot - at < 2 || mail.IndexOf('@', at + 1) != -1) { throw new ArgumentException("В адресе электронной почты допущена ошибка", nameof(mail)); }

			return (mail.Substring(0, at), mail.Substring(at + 1, dot - at - 1), mail.Substring(dot + 1));
		}

		///<summary>Парсинг электронной почты</summary>
		///<param name="address">Имя электронной почты</param>
		///<param name="host">Хост электронной почты</param>
		private (string address, string domain, string zone) parseMail(string address, string host)
		{
			var components = parseMail($"{address}@{host}");
			return (components.address, components.domain, components.zone);
		}

		///<summary>Валидация электронной почты</summary>
		///<param name="component">Компонент адреса электронной почты</param>
		///<param name="isZone">Является ли аргумент зоной адреса электронной почты</param>
		private Exception validation(string component, bool isZone)
		{
			component = component.Trim().ToLower();
			if (string.IsNullOrEmpty(component)) { return new ArgumentNullException("Компоненты электронной почты не могут быть представлены пустой строкой", nameof(component)); }

			for (int letter = 0; letter < component.Length; letter++)
			{
				if (isZone && component[letter] > 96 && component[letter] < 123) { continue; }
				else if (!isZone && ((component[letter] > 96 && component[letter] < 123) || char.IsDigit(component[letter]))) { continue; }
				else if (!isZone && (component[letter] == '.' || component[letter] == '-' || component[letter] == '_') && letter > 0 && letter < component.Length - 1 && char.IsLetterOrDigit(component[letter - 1]) && char.IsLetterOrDigit(component[letter + 1])) { continue; }
				else { return new ArgumentException("В адресе электронной почты допущена ошибка", nameof(component)); }
			}
			return null;
		}

		///<summary>Валидация</summary>
		///<param name="component">Компонент адреса электронной почты</param>
		private Exception validation(string component) => validation(component, false);

		///<summary>Возвращает строковое представление адреса электронной почты после нормализации</summary>
		///<param name="component">Компонент адреса электронной почты</param>
		private string normalization(string component) => component.Trim().ToLower();

		///<summary>Строковое представление текущего экземпляра</summary>
		public override string ToString() => _address + "@" + Host;

		///<summary>Выполняет отправку сообщения на электронную почту</summary>
		///<param name="_from">Логин почты отправителя</param>
		///<param name="_passFrom">Пароль почты отправителя</param>
		///<param name="_to">Логин почты получателя</param>
		///<param name="messege">Сообщение</param>
		///<param name="title">Тема сообщения</param>
		public void Send(string _passFrom, Mail _to, string messege, string title)
		{
			MailAddress from = new MailAddress($"{_mail}", "Минздрав");
			MailAddress to = new MailAddress(_to);
			MailMessage m = new MailMessage(from, to);
			m.Subject = title;
			m.Body = messege;

			m.IsBodyHtml = false;
			SmtpClient smtp = new SmtpClient("smtp.mail.ru", 25);

			smtp.Credentials = new NetworkCredential($"{_mail}", _passFrom);
			smtp.EnableSsl = true;
			smtp.Send(m);
		}
		#endregion

		#region " Операторы "

		public static bool operator ==(Mail a, Mail b) => a._address == b._address && a._domain == b._domain && a._zone == b._zone;
		public static bool operator !=(Mail a, Mail b) => a._address != b._address || a._domain != b._domain || a._zone != b._zone;

		public static bool operator true(Mail a) => a != null;
		public static bool operator false(Mail a) => a == null;
		public static bool operator !(Mail a) => a == null;

		public static implicit operator string(Mail a) => a.ToString();

		#endregion

	}

}
