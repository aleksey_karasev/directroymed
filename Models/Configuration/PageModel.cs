﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models
{
    public class PageModel<T>
    {
        public List<T> List { get; set; }
        public int Total { get; set; }
        public int Length { get; set; }
    }
}
