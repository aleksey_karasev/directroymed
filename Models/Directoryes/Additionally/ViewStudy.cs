﻿using Directory.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.Directoryes.Additionally
{
	///<summary>Описывает вид исследования, выполняет роль справочника</summary>
	public class ViewStudy : Entity
	{
		[DisplayName("Вида исследования")]
		public string Name { get; set; }    //Название вида исследования

		[DisplayName("Заблокирован")]
		public bool LockedSection { get; set; }

		public string PersonId { get; set; }                   //Автор вида

	}
}
