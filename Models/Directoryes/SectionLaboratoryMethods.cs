﻿using Directory.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.Directoryes
{
	///<summary>Характеризует Раздел для <LaboratoryMethods> лабораторных методов</summary>
	public class SectionLaboratoryMethods : Entity
	{
		[DisplayName("Название раздела")]
		public string Name { get; set; } //Название раздела 

		[DisplayName("Заблокирован")]
		public bool LockedSection { get; set; }

		public string PersonId { get; set; }                   //Автор раздела
	}
}
