﻿using Directory.Models.Directoryes;
using Directory.Models.Directoryes.Additionally;
using Directory.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.DirectoryesMedic
{
	///<summary>Описывает медицинский справочник "Методов медицинской реабилитации"</summary>
	public class MedicRehabilitation : Entity
	{
		public string ViewStudy { get; set; }                //Вид исследования

		public string SectionCode { get; set; }             //Номер раздела в формате с нулем #00

		public string SectionName { get; set; }             //Название раздела

		public string OrdinalNumber { get; set; }           //Порядковый номер

		public string CodeStudy { get; set; }               //Код исследования

		public string NameStude { get; set; }               //Название исследования

		public int SectionId { get; set; }                  //Хранимое айди радела

		public SectionMedicRehabilitation Section { get; set; }    //Раздел (для обращения вместо конструктора)

		public string PersonId { get; set; }                   //Автор справочника
	}
}
