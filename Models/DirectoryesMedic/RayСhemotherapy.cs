﻿using Directory.Models.Directoryes;
using Directory.Models.Directoryes.Additionally;
using Directory.Models.Identity;

namespace Directory.Models.DirectoryesMedic
{
	///<summary>Описывает медицинский справочник "Лучевой химиотерапии"</summary>
	public class RayСhemotherapy : Entity
	{
		public string ViewStudy { get; set; }            //Вид исследования

		public string SectionCode { get; set; }             //Номер раздела в формате с нулем #00

		public string SectionName { get; set; }             //Название раздела

		public string OrdinalNumber { get; set; }           //Порядковый номер

		public string CodeStudy { get; set; }               //Код исследования

		public string NameStude { get; set; }               //Название исследования

		public int SectionId { get; set; }                  //Хранимое айди радела

		public SectionRayСhemotherapy Section { get; set; }    //Раздел (для обращения вместо конструктора)

		public string PersonId { get; set; }                   //Автор справочника
	}
}
