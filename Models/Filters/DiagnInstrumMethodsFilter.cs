﻿using Directory.Models.ModelsView.DirectoryesMedic;
using System.Collections.Generic;

namespace Directory.Models.Filters
{
	public class DiagnInstrumMethodsFilter 
	{
		public int? Id { get; set; }

		public int SectionCode { get; set; }				//Номер раздела

		public string SectionName { get; set; }				//Название раздела

		public string CodeStudy { get; set; }				//Номер исследования

		public string NameStude { get; set; }				//Название исследования

		public List<DIMViewModel> DIMethods { get; set; }	//Список найденных справочников
	}
}
