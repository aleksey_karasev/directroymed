﻿using Directory.Models.ModelsView.DirectoryesMedic;
using System.Collections.Generic;

namespace Directory.Models.Filters
{
	public class LaboratoryMethodsFilter
	{
		public int? Id { get; set; }

		public int ViewStudy { get; set; }                  //Вид исследования

		public int SectionCode { get; set; }                //Номер раздела

		public string SectionName { get; set; }             //Название раздела

		public string CodeStudy { get; set; }               //Номер исследования

		public string NameStude { get; set; }               //Название исследования

		public List<LMViewModel> LMMethods { get; set; }   //Список найденных справочников
	}
}
