﻿using Directory.Tokens;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Directory.Models.Identity
{
	public class Person : IdentityUser
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string MiddleName { get; set; }

		public string NameSystem { get; set; }

		public string Grounding { get; set; }

		public int[] DirectoryMedicId { get; set; }

		public DirectoryMedic DirectoryMedic { get; set; }

		public bool isApplicationForm { get; set; }

		public int TokenId { get; set; }

		public Person()
		{
			PersonRole = new List<PersonRole>();
		}

		public virtual IList<PersonRole> PersonRole { get; set; }
	}
}
