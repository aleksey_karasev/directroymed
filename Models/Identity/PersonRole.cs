﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.Identity
{
	public class PersonRole : IdentityUserRole<string>
	{
		public override string RoleId { get; set; }
		public virtual Role Role { get; set; }

		public override string UserId { get; set; }
		public virtual Person User { get; set; }
	}
}
