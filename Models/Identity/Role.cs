﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.Identity
{
	public class Role : IdentityRole
	{
		public Role()
		{
			PersonRoles = new List<PersonRole>();
		}

		public virtual IList<PersonRole> PersonRoles { get; set; }
	}
}
