﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.ModelsView
{
	public class ChangePasswordViewModel
	{
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        [Required(ErrorMessage = "Введите текущий пароль")]
        public string OldPassword { get; set; }

        [Display(Name = "Новый пароль")]
        [Required(ErrorMessage = "Введите новый пароль")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Пароли должны состоять не менее 8 символов", MinimumLength = 8)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Повторите новый пароль")]
        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Повторите новый пароль")]
        public string PasswordConfirm { get; set; }
    }
}
