﻿using Directory.Models.Directoryes;
using Directory.Models.Directoryes.Additionally;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Directory.Models.ModelsView.DirectoryesMedic
{
	///<summary>Описывает медицинский справочник "Лучевой химиотерапии"</summary>
	public class RCViewModel
	{
		public int Id { get; set; }

		[DisplayName("Вид исследования")]
		public string ViewStudy { get; set; }                //Вид исследования

		[Required(ErrorMessage = "Раздел обязателен!")]
		[DisplayName("Код раздела")]
		public string SectionCode { get; set; }                 //Код раздела

		[DisplayName("Название раздела")]
		public string SectionName { get; set; }                 //Раздел

		[DisplayName("Порядковый номер")]
		public string OrdinalNumber { get; set; }               //Порядковый номер

		[DisplayName("Код исследования")]
		public string CodeStudy { get; set; }                   //Код исследования

		[Required(ErrorMessage = "Название исследования обязателено!")]
		[DisplayName("Название исследования")]
		public string NameStude { get; set; }                   //Название исследования

		[DisplayName("Раздел")]
		public int SectionId { get; set; }                  //Хранимое айди радела

		public SectionDiagnInstrum Section { get; set; }    //Раздел (для обращения вместо конструктора)
	}
}
