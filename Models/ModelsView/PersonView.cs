﻿using Directory.Models.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Directory.Models.ModelsView
{
	public class PersonView
	{
		public string Id { get; set; }

		[DataType(DataType.Text)]
		[Required(ErrorMessage = "Поле является обязательным к заполнению")]
		[Display(Name = "Имя")]
		public string FirstName { get; set; }

		[DataType(DataType.Text)]
		[Required(ErrorMessage = "Поле является обязательным к заполнению")]
		[Display(Name = "Фамилия")]
		public string LastName { get; set; }

		[DataType(DataType.Text)]
		[Display(Name = "Отчество")]
		public string MiddleName { get; set; }

		//[DataType(DataType.Text)]
		//[Required(ErrorMessage = "Поле является обязательным к заполнению")]
		//[Display(Name = "Логин")]
		//public string UserName { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Пароль")]
		public string Password { get; set; }

		[DataType(DataType.PhoneNumber)]
		[Display(Name = "Телефон")]
		public string PhoneNumber { get; set; }

		[DataType(DataType.Text)]
		[Required(ErrorMessage = "Поле является обязательным к заполнению")]
		[Display(Name = "Название системы")]
		public string NameSystem { get; set; }

		[EmailAddress]
		[Required(ErrorMessage = "Поле является обязательным к заполнению")]
		[Display(Name = "Эл. Почта")]
		public string Email { get; set; }

		[DataType(DataType.Text)]
		[Required(ErrorMessage = "Поле является обязательным к заполнению")]
		[Display(Name = "Обоснование")]
		public string Grounding { get; set; }

		[Display(Name = "Права (роль) пользователя")]
		public List<Role> RoleId { get; set; }

		[Display(Name = "Заблокирован")]
		public bool LockoutEnabled { get; set; }

		[Display(Name = "Это Анкета")]
		public bool isApplicationForm { get; set; }

		public PersonView()
		{
			RoleId = new List<Role>();
		}
	}
}