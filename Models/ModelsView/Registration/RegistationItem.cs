﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.ModelsView.Registration
{
	public class RegistationItem : Entity
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string MiddleName { get; set; }

		public string UserName { get; set; }

		public string PasswordHash { get; set; }

		public string PhoneNumber { get; set; }

		public string NameSystem { get; set; }

		public string Email { get; set; }

		public string Grounding { get; set; }

		public Access StatusAccess { get; set; }
	}
}
