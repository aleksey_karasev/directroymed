﻿using Directory.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Models.ModelsView
{
	public class RoleView
	{
        public string UserId { get; set; }
        public string UserName { get; set; }
        public List<Role> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }

        public RoleView()
        {
            AllRoles = new List<Role>();
            UserRoles = new List<string>();
        }
    }
}
