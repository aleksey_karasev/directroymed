﻿using Directory.Models;
using Directory.ModelsConfiguraion;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.ModelsConfiguraion
{
	public abstract class BaseEntityConfiguration<T> : IEntityConfiguration<T> where T : Entity
	{
		public Action<EntityTypeBuilder<T>> ProvideConfigurationAction() { return ConfigureEntity; }

		///<summary>Характеризует сущность</summary>
		///<param name="bulder"></param>
		protected void ConfigureEntity(EntityTypeBuilder<T> builder)
		{
			ConfigureProperties(builder); ConfigurePrimaryKeys(builder); ConfigureForeignKeys(builder);
		}

		///<summary>Характеризует свойства сущности</summary>
		///<param name="bulder"></param>
		protected virtual void ConfigureProperties(EntityTypeBuilder<T> builder) { }

		///<summary>Характеризует внутренний ключ, который можно будет передать к унаследованным сущностям</summary>
		///<param name="bulder"></param>
		protected virtual void ConfigurePrimaryKeys(EntityTypeBuilder<T> builder) { builder.HasKey(e => e.id); }

		///<summary>Характеризует внешний ключ, через который можно будет взаимодествовать с сущностью</summary>
		///<param name="bulder"></param>
		protected virtual void ConfigureForeignKeys(EntityTypeBuilder<T> builder) { }
	}
}
