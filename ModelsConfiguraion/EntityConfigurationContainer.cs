﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models;
using Directory.Models.Directoryes;
using Directory.Models.Directoryes.Additionally;
using Directory.Models.DirectoryesMedic;
using Directory.Tokens;

namespace Directory.ModelsConfiguraion
{
	public class EntityConfigurationContainer : IEntityConfigurationContainer
	{
		public IEntityConfiguration<DiagnInstrumMethods> DiagnInstrumMethodsConfiguration { get; }

		public IEntityConfiguration<LaboratoryMethods> LaboratoryMethodsConfiguration { get; }

		public IEntityConfiguration<MedicRehabilitation> MedicRehabilitationConfiguration { get; }

		public IEntityConfiguration<OutpatientPolyclinicLevel> OutpatientPolyclinicLevelConfiguration { get; }

		public IEntityConfiguration<RayСhemotherapy> RayСhemotherapyConfiguration { get; }

		public IEntityConfiguration<SurgicalOperations> SurgicalOperationsConfiguration { get; }


		public IEntityConfiguration<SectionDiagnInstrum> SectionDIMConfiguration { get; }

		public IEntityConfiguration<SectionLaboratoryMethods> SectionLMConfiguration { get; }

		public IEntityConfiguration<SectionMedicRehabilitation> SectionMRConfiguration { get; }

		public IEntityConfiguration<SectionOutpatientPolyclinicLevel> SectionOPLConfiguration { get; }

		public IEntityConfiguration<SectionRayСhemotherapy> SectionRCConfiguration { get; }

		public IEntityConfiguration<SectionSurgicalOperations> SectionSOConfiguration { get; }

		public IEntityConfiguration<ViewStudy> ViewStudyConfiguration { get; }

		public IEntityConfiguration<Token> TokenConfiguration { get; }


		public EntityConfigurationContainer()
		{

			DiagnInstrumMethodsConfiguration = new DiagnInstrumMethodsConfiguration();

			LaboratoryMethodsConfiguration = new LaboratoryMethodsConfiguration();

			MedicRehabilitationConfiguration = new MedicRehabilitationConfiguration();

			OutpatientPolyclinicLevelConfiguration = new OutpatientPolyclinicLevelConfiguration();

			RayСhemotherapyConfiguration = new RayСhemotherapyConfiguration();

			SurgicalOperationsConfiguration = new SurgicalOperationsConfiguration();


			SectionDIMConfiguration = new SectionDIMConfiguration();

			SectionLMConfiguration = new SectionLMConfiguration();

			SectionMRConfiguration = new SectionMRConfiguration();

			SectionOPLConfiguration = new SectionOPLConfiguration();

			SectionRCConfiguration = new SectionRCConfiguration();

			SectionSOConfiguration = new SectionSOConfiguration();


			ViewStudyConfiguration = new ViewStudyConfiguration();

			TokenConfiguration = new TokenConfiguration();
		}

	}
}
