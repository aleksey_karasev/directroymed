﻿using Directory.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.ModelsConfiguraion
{
	public interface IEntityConfiguration<T> where T : Entity
	{
		Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
	}
}
