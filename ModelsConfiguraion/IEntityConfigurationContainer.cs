﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models;
using Directory.Models.Directoryes;
using Directory.Models.Directoryes.Additionally;
using Directory.Models.DirectoryesMedic;
using Directory.Tokens;

namespace Directory.ModelsConfiguraion
{
	public interface IEntityConfigurationContainer
	{
		IEntityConfiguration<DiagnInstrumMethods> DiagnInstrumMethodsConfiguration { get; }

		IEntityConfiguration<LaboratoryMethods> LaboratoryMethodsConfiguration { get; }

		IEntityConfiguration<MedicRehabilitation> MedicRehabilitationConfiguration { get; }

		IEntityConfiguration<OutpatientPolyclinicLevel> OutpatientPolyclinicLevelConfiguration { get; }

		IEntityConfiguration<RayСhemotherapy> RayСhemotherapyConfiguration { get; }

		IEntityConfiguration<SurgicalOperations> SurgicalOperationsConfiguration { get; }


		IEntityConfiguration<SectionDiagnInstrum> SectionDIMConfiguration { get; }

		IEntityConfiguration<SectionLaboratoryMethods> SectionLMConfiguration { get; }

		IEntityConfiguration<SectionMedicRehabilitation> SectionMRConfiguration { get; }

		IEntityConfiguration<SectionOutpatientPolyclinicLevel> SectionOPLConfiguration { get; }

		IEntityConfiguration<SectionRayСhemotherapy> SectionRCConfiguration { get; }

		IEntityConfiguration<SectionSurgicalOperations> SectionSOConfiguration { get; }


		IEntityConfiguration<ViewStudy> ViewStudyConfiguration { get; }

		IEntityConfiguration<Token> TokenConfiguration { get; }

	}
}
