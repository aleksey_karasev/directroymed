﻿using Directory.Models.DirectoryesMedic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.ModelsConfiguraion
{
	public class OutpatientPolyclinicLevelConfiguration : BaseEntityConfiguration<OutpatientPolyclinicLevel>
	{
		protected override void ConfigureProperties(EntityTypeBuilder<OutpatientPolyclinicLevel> builder)
		{
			builder.Property(p => p.SectionName).HasMaxLength(255).IsRequired();
			builder.Property(p => p.SectionCode).IsRequired();
			builder.HasIndex(p => p.id).IsUnique();
		}

		//Пользователь не зависит ни от кого, и у него нет вторичного ключа
		protected override void ConfigureForeignKeys(EntityTypeBuilder<OutpatientPolyclinicLevel> builder)
		{
			base.ConfigureForeignKeys(builder);
		}
	}
}
