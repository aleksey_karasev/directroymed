﻿using Directory.Models.Directoryes;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.ModelsConfiguraion
{
	public class SectionRCConfiguration : BaseEntityConfiguration<SectionRayСhemotherapy>
	{
		protected override void ConfigureProperties(EntityTypeBuilder<SectionRayСhemotherapy> builder)
		{
			builder.Property(p => p.Name).HasMaxLength(255).IsRequired();
			builder.HasIndex(p => p.id).IsUnique();
		}

		//Пользователь не зависит ни от кого, и у него нет вторичного ключа
		protected override void ConfigureForeignKeys(EntityTypeBuilder<SectionRayСhemotherapy> builder)
		{
			base.ConfigureForeignKeys(builder);
		}
	}
}
