﻿using Directory.Tokens;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.ModelsConfiguraion
{
	public class TokenConfiguration : BaseEntityConfiguration<Token>
	{
		protected override void ConfigureProperties(EntityTypeBuilder<Token> builder)
		{
			builder.Property(p => p.Person).IsRequired();
			builder.Property(p => p.Tokens).IsRequired();
			builder.HasIndex(p => p.id).IsUnique();
		}

		//Пользователь не зависит ни от кого, и у него нет вторичного ключа
		protected override void ConfigureForeignKeys(EntityTypeBuilder<Token> builder)
		{
			base.ConfigureForeignKeys(builder);
		}
	}
}
