﻿using Directory.Data;
using Directory.Models.DirectoryesMedic;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class DiagnInstrumMethodsRepository : Repository<DiagnInstrumMethods>, IDiagnInstrumMethodsRepository
	{
		public DiagnInstrumMethodsRepository(ApplicationDbContext context) : base(context) { entities = context.DiagnInstrumMethods; }

		public List<DiagnInstrumMethods> GetDiagnInstrumMethods() { return entities.ToList(); }
	}
}
