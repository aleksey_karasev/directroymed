﻿using Directory.Models.DirectoryesMedic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Repositories.Interfaces
{
	public interface IDiagnInstrumMethodsRepository : IRepository<DiagnInstrumMethods>
	{
		List<DiagnInstrumMethods> GetDiagnInstrumMethods();
	}
}
