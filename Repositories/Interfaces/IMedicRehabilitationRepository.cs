﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.DirectoryesMedic;

namespace Directory.Repositories.Interfaces
{
	public interface IMedicRehabilitationRepository : IRepository<MedicRehabilitation>
	{
		List<MedicRehabilitation> GetMedicRehabilitation();
	}
}
