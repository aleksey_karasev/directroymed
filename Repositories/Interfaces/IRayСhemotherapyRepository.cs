﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.DirectoryesMedic;

namespace Directory.Repositories.Interfaces
{
	public interface IRayСhemotherapyRepository : IRepository<RayСhemotherapy>
	{
		List<RayСhemotherapy> GetRayСhemotherapy();
	}
}
