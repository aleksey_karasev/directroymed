﻿using Directory.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.DirectoryesMedic;

namespace Directory.Repositories.Interfaces
{
	///<summary>Интерфейс, который описывает основные инструменты управления с БД</summary>
	public interface IRepository<T> where T : Entity
	{
		T Create(T entity);         //Создание Таблицы

		T GetById(int id);			//Ищет сущность по id (хэшированному)

		IEnumerable<T> GetAll();	//Достать все из таблицы

		T Update(T entity);			//Изменить таблицу (не данные а поля)

		void Remove(T entity);		//Удалить таблицу

	}
}
