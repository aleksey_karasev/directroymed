﻿using Directory.Models.Directoryes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Repositories.Interfaces
{
	public interface ISectionDIMRepository : IRepository<SectionDiagnInstrum>
	{
		List<SectionDiagnInstrum> GetSectionDIM();
	}
}
