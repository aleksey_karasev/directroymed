﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Directoryes;

namespace Directory.Repositories.Interfaces
{
	public interface ISectionMRRepository : IRepository<SectionMedicRehabilitation>
	{
		List<SectionMedicRehabilitation> GetSectionMR();
	}
}
