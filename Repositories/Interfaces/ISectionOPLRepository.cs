﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Directoryes;
using Directory.Models.DirectoryesMedic;

namespace Directory.Repositories.Interfaces
{
	public interface ISectionOPLRepository : IRepository<SectionOutpatientPolyclinicLevel>
	{
		List<SectionOutpatientPolyclinicLevel> GetSectionOPL();
	}
}
