﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Directoryes;

namespace Directory.Repositories.Interfaces
{
	public interface ISectionSORepository : IRepository<SectionSurgicalOperations>
	{
		List<SectionSurgicalOperations> GetSectionSO();
	}
}
