﻿using Directory.Models.Directoryes.Additionally;
using Directory.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Repositories.Interfaces
{
	public interface ITokenRepository : IRepository<Token>
	{
		List<Token> GetToken();
	}
}
