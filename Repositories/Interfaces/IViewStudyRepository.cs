﻿using Directory.Models.Directoryes.Additionally;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Repositories.Interfaces
{
	public interface IViewStudyRepository : IRepository<ViewStudy>
	{
		List<ViewStudy> GetViewStudy();
	}
}
