﻿using Directory.Data;
using Directory.Models.DirectoryesMedic;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class LaboratoryMethodsRepository : Repository<LaboratoryMethods>, ILaboratoryMethodsRepository
	{
		public LaboratoryMethodsRepository(ApplicationDbContext context) : base(context) { entities = context.LaboratoryMethods; }

		public List<LaboratoryMethods> GetLaboratoryMethods() { return entities.ToList(); }
	}
}
