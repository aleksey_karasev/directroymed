﻿using Directory.Data;
using Directory.Models.DirectoryesMedic;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class MedicRehabilitationRepository : Repository<MedicRehabilitation>, IMedicRehabilitationRepository
	{
		public MedicRehabilitationRepository(ApplicationDbContext context) : base(context) { entities = context.MedicRehabilitation; }

		public List<MedicRehabilitation> GetMedicRehabilitation() { return entities.ToList(); }
	}
}
