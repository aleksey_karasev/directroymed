﻿using Directory.Data;
using Directory.Models.DirectoryesMedic;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class OutpatientPolyclinicLevelRepository : Repository<OutpatientPolyclinicLevel>, IOutpatientPolyclinicLevelRepository
	{
		public OutpatientPolyclinicLevelRepository(ApplicationDbContext context) : base(context) { entities = context.OutpatientPolyclinicLevel; }

		public List<OutpatientPolyclinicLevel> GetOutpatientPolyclinicLevel() { return entities.ToList(); }
	}
}
