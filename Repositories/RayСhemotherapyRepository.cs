﻿using Directory.Data;
using Directory.Models.DirectoryesMedic;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class RayСhemotherapyRepository : Repository<RayСhemotherapy>, IRayСhemotherapyRepository
	{
		public RayСhemotherapyRepository(ApplicationDbContext context) : base(context) { entities = context.RayСhemotherapy; }

		public List<RayСhemotherapy> GetRayСhemotherapy() { return entities.ToList(); }
	}
}
