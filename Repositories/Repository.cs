﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models;
using Directory.Data;
using Microsoft.AspNetCore.Identity;
using Directory.Repositories.Interfaces;

namespace Directory.Repositories
{
	public class Repository<T> : IRepository<T> where T : Entity
	{
		private ApplicationDbContext _context;
		protected DbSet<T> entities;

		public Repository(ApplicationDbContext context) { _context = context; }

		public T Create(T entity) { entities.Add(entity); _context.SaveChanges(); return entity; }

		public T GetById(int id) { return entities.FirstOrDefault(e => e.id == id); }

		public virtual IEnumerable<T> GetAll() { return entities;}

		public void Remove(T entity) { entities.Remove(entity); _context.SaveChanges(); }

		public T Update(T entity) { _context.Update(entity); _context.SaveChanges(); return entity; }
	}
}
