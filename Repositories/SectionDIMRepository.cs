﻿using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Models.DirectoryesMedic;
using Directory.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Repositories
{
	public class SectionDIMRepository : Repository<SectionDiagnInstrum>, ISectionDIMRepository
	{
		public SectionDIMRepository(ApplicationDbContext context) : base(context) { entities = context.SectionDiagnInstrum; }

		public List<SectionDiagnInstrum> GetSectionDIM() { return entities.ToList(); }
	}
}
