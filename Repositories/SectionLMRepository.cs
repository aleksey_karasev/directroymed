﻿using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class SectionLMRepository : Repository<SectionLaboratoryMethods>, ISectionLMRepository
	{
		public SectionLMRepository(ApplicationDbContext context) : base(context) { entities = context.SectionLaboratoryMethods; }

		public List<SectionLaboratoryMethods> GetSectionLM() { return entities.ToList(); }
	}
}
