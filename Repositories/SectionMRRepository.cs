﻿using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class SectionMRRepository : Repository<SectionMedicRehabilitation>, ISectionMRRepository
	{
		public SectionMRRepository(ApplicationDbContext context) : base(context) { entities = context.SectionMedicRehabilitation; }

		public List<SectionMedicRehabilitation> GetSectionMR() { return entities.ToList(); }
	}
}
