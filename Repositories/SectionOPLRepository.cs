﻿using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class SectionOPLRepository : Repository<SectionOutpatientPolyclinicLevel>, ISectionOPLRepository
	{
		public SectionOPLRepository(ApplicationDbContext context) : base(context) { entities = context.SectionOutpatientPolyclinicLevel; }

		public List<SectionOutpatientPolyclinicLevel> GetSectionOPL() { return entities.ToList(); }
	}
}
