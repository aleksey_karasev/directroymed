﻿using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class SectionRCRepository : Repository<SectionRayСhemotherapy>, ISectionRCRepository
	{
		public SectionRCRepository(ApplicationDbContext context) : base(context) { entities = context.SectionRayСhemotherapy; }

		public List<SectionRayСhemotherapy> GetSectionRC() { return entities.ToList(); }
	}
}
