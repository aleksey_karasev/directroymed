﻿using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class SectionSORepository : Repository<SectionSurgicalOperations>, ISectionSORepository
	{
		public SectionSORepository(ApplicationDbContext context) : base(context) { entities = context.SectionSurgicalOperations; }

		public List<SectionSurgicalOperations> GetSectionSO() { return entities.ToList(); }
	}
}
