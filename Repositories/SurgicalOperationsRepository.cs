﻿using Directory.Data;
using Directory.Models.DirectoryesMedic;
using Directory.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Directory.Repositories
{
	public class SurgicalOperationsRepository : Repository<SurgicalOperations>, ISurgicalOperationsRepository
	{
		public SurgicalOperationsRepository(ApplicationDbContext context) : base(context) { entities = context.SurgicalOperations; }

		public List<SurgicalOperations> GetSurgicalOperations() { return entities.ToList(); }
	}
}
