﻿using Directory.Data;
using Directory.Models.Directoryes.Additionally;
using Directory.Repositories.Interfaces;
using Directory.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Repositories
{
	public class TokenRepository : Repository<Token>, ITokenRepository
	{
		public TokenRepository(ApplicationDbContext context) : base(context) { entities = context.Token; }

		public List<Token> GetToken() { return entities.ToList(); }
	}
}
