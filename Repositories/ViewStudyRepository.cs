﻿using Directory.Data;
using Directory.Models.Directoryes.Additionally;
using Directory.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Repositories
{
	public class ViewStudyRepository : Repository<ViewStudy>, IViewStudyRepository
	{
		public ViewStudyRepository(ApplicationDbContext context) : base(context) { entities = context.ViewStudy; }

		public List<ViewStudy> GetViewStudy() { return entities.ToList(); }
	}
}
