﻿using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services.abstractClasses
{
	public class SplitDIM
	{
		private DiagnInstrumMethods _DIM;
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly ISectionDIMServices _sectionDIMServices;
		private readonly DiagnInstrumMethodsServices _diagnInstrumMethodsServices;
		private readonly IHttpContextAccessor _httpContextAccessor;		//Текущий пользователь

		public SplitDIM(DiagnInstrumMethods DIM, DiagnInstrumMethodsServices diagnInstrumMethodsServices, IHttpContextAccessor httpContextAccessor, bool craete)
		{
			if (diagnInstrumMethodsServices is null || DIM is null) { throw new ArgumentNullException("connection is null", nameof(diagnInstrumMethodsServices)); }

			_DIM = DIM;
			_diagnInstrumMethodsServices = diagnInstrumMethodsServices;
			_unitOfWorkFactory = _diagnInstrumMethodsServices.GetUnitOfWorkFactory;
			_sectionDIMServices = _diagnInstrumMethodsServices.GetSectionDIMServices;
			_httpContextAccessor = httpContextAccessor;

			SpitDir(craete);
		}

		public string OrdinalNumber => _DIM.OrdinalNumber;
		public string SectionCode => _DIM.SectionCode;
		public string CodeStudy => _DIM.CodeStudy;
		public int SectionId => _DIM.SectionId;
		public string SectionName => _DIM.SectionName;


		private void SpitDir(bool create)
		{
			string code = "000";

			try
			{
				//Достаю все текущие справочники, и ищу из них те, которые совподают с текущим разделом (Section).
				//Это делается для автоматического заполнения порядкого номера (OrdinalNumber), 
				//Так как он соответсвует последней записи текущего раздела
				//пример: Section == 1? last OrdinalNumber == 34 ? то текущий становится 34 + 1 = 35.

				var dimWithTheSection = _diagnInstrumMethodsServices.FilterDiagnInstrumMethods(null);

				dimWithTheSection = dimWithTheSection.Where(e => e.SectionCode.Trim('0') == _DIM.SectionCode).ToList();

				//Если первая запись в текущем разделе
				if (dimWithTheSection.Count() == 0) { _DIM.OrdinalNumber = "1"; }

				//Получаю последнюю запись порядкого номера, и изменяю порядковый номер, исходя от того, изменяется ли 
				else { _DIM.OrdinalNumber = (int.Parse(dimWithTheSection.Last().OrdinalNumber) + (create || dimWithTheSection.Where(e => e.NameStude == _DIM.NameStude).Count() == 0 && !create ? 1 : 0)).ToString(); }

				//Добавляю нули в соответствии с форматом, и прибавляю + 1 к поряд. номеру.
				_DIM.OrdinalNumber = $"{code.Remove(0, _DIM.OrdinalNumber.Trim('0').Length)}{_DIM.OrdinalNumber}";


				//Вытаскиваю исследование из БД по айди
				_DIM.Section = _sectionDIMServices.GetSectionById(int.Parse(_DIM.SectionCode));

				//Записываю исследование в правильный формат
				_DIM.SectionCode = $"{code.Remove(0, _DIM.SectionCode.Trim('0').Length + 1)}{_DIM.Section.id}";

				//Заполняю код исследования
				_DIM.CodeStudy = $"D{_DIM.SectionCode}.{_DIM.OrdinalNumber}";

				//Записываю айди к внешнему ключу 
				_DIM.SectionId = _DIM.Section.id;

				_DIM.SectionName = _DIM.Section.Name;

				_DIM.Section = null; //Он должен быть пустым обязательно

				_DIM.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);

				//Подключаюсь к БД
				using (var unitOfWork = _unitOfWorkFactory.Create())
				{
					if (create) { unitOfWork.DiagnInstrumMethods.Create(_DIM); }
					else { unitOfWork.DiagnInstrumMethods.Update(_DIM); }
				}
			}
			catch { }
		}
	}
}
