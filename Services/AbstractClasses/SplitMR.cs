﻿using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services.abstractClasses
{
	public class SplitMR
	{
		private MedicRehabilitation _dir;
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly ISectionMRServices _sectionMRServices;
		private readonly MedicRehabilitationServices _services;
		private readonly ViewStudyServices _viewStudyServices;
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SplitMR(MedicRehabilitation dir, MedicRehabilitationServices services, IHttpContextAccessor httpContextAccessor, bool craete)
		{
			if (dir is null || services is null) { throw new ArgumentNullException("connection is null", nameof(services)); }

			_dir = dir;
			_services = services;
			_unitOfWorkFactory = _services.GetUnitOfWorkFactory;
			_sectionMRServices = _services.GetSectionMRServices;
			_viewStudyServices = new(_unitOfWorkFactory, _httpContextAccessor);
			_httpContextAccessor = httpContextAccessor;

			SpitDir(craete);
		}

		public string OrdinalNumber => _dir.OrdinalNumber;
		public string SectionCode => _dir.SectionCode;
		public string CodeStudy => _dir.CodeStudy;
		public int SectionId => _dir.SectionId;
		public string SectionName => _dir.SectionName;


		private void SpitDir(bool create)
		{
			string code = "000";

			try
			{
				//Достаю все текущие справочники, и ищу из них те, которые совподают с текущим разделом (Section).
				//Это делается для автоматического заполнения порядкого номера (OrdinalNumber), 
				//Так как он соответсвует последней записи текущего раздела
				//пример: Section == 1? last OrdinalNumber == 34 ? то текущий становится 34 + 1 = 35.

				var dimWithTheSection = _services.FilterMedicRehabilitation(null);

				dimWithTheSection = dimWithTheSection.Where(e => e.SectionCode.Trim('0') == _dir.SectionCode).ToList();

				//Если первая запись в текущем разделе
				if (dimWithTheSection.Count() == 0) { _dir.OrdinalNumber = "1"; }

				//Получаю последнюю запись порядкого номера
				else { _dir.OrdinalNumber = (int.Parse(dimWithTheSection.Last().OrdinalNumber) + (create || dimWithTheSection.Where(e => e.NameStude == _dir.NameStude).Count() == 0 && !create ? 1 : 0)).ToString(); }

				//Добавляю нули в соответствии с форматом, и прибавляю + 1 к поряд. номеру.
				_dir.OrdinalNumber = $"{code.Remove(0, _dir.OrdinalNumber.Trim('0').Length)}{_dir.OrdinalNumber}";


				_dir.ViewStudy = _viewStudyServices.GetViewStudyById(int.Parse(_dir.ViewStudy)).Name;

				//Вытаскиваю исследование из БД по айди
				_dir.Section = _sectionMRServices.GetSectionById(int.Parse(_dir.SectionCode));

				//Записываю исследование в правильный формат
				_dir.SectionCode = $"{code.Remove(0, _dir.SectionCode.Trim('0').Length + 1)}{_dir.Section.id}";

				//Заполняю код исследования
				_dir.CodeStudy = $"D{_dir.SectionCode}.{_dir.OrdinalNumber}";

				//Записываю айди к внешнему ключу 
				_dir.SectionId = _dir.Section.id;

				_dir.SectionName = _dir.Section.Name;

				_dir.Section = null; //Он должен быть пустым обязательно

				_dir.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);

				//Подключаюсь к БД
				using (var unitOfWork = _unitOfWorkFactory.Create())
				{
					if (create) { unitOfWork.MedicRehabilitation.Create(_dir); }
					else { unitOfWork.MedicRehabilitation.Update(_dir); }
				}
			}
			catch { }
		}
	}
}
