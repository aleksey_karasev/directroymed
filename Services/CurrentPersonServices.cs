﻿using Directory.Data.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services.interfaces
{
	public class CurrentPersonServices : ICurrentPersonServices
	{
		public string Id { get; set; }
		public string FullName { get; set; }
		public bool IsRoot { get; set; }
		public string DirectoryName { get; set; }
		public int DirectoryId { get; set; }

		public CurrentPersonServices(IHttpContextAccessor httpContextAccessor)
		{
			Id = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);

			FullName = httpContextAccessor.HttpContext?.User?.FindFirstValue(CustomClaimTypes.FullName);

			//string isRoot = httpContextAccessor.HttpContext?.User?.FindFirstValue(CustomClaimTypes.IsRoot);
			//if (!string.IsNullOrEmpty(isRoot))
			//	IsRoot = Convert.ToBoolean(isRoot);

			DirectoryName = httpContextAccessor.HttpContext?.User?.FindFirstValue(CustomClaimTypes.DirectoryName);

			string personDirectoryId = httpContextAccessor.HttpContext?.User?.FindFirstValue(CustomClaimTypes.DirectoryId);
			if (!string.IsNullOrEmpty(personDirectoryId))
				DirectoryId = Convert.ToInt32(personDirectoryId);
		}

	}
}
