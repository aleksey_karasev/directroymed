﻿using AutoMapper;
using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Directory.Services.abstractClasses;
using Microsoft.AspNetCore.Http;

namespace Directory.Services
{
	public class DiagnInstrumMethodsServices : IDiagnInstrumMethodsServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly ISectionDIMServices _sectionDIMServices;
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		#region "Конструкторы"

		public DiagnInstrumMethodsServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, ISectionDIMServices sectionDIMServices, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _sectionDIMServices = sectionDIMServices; _httpContextAccessor = httpContextAccessor;
		}

		#endregion

		#region "Свойства"

		public IUnitOfWorkFactory GetUnitOfWorkFactory => _unitOfWorkFactory;

		public ISectionDIMServices GetSectionDIMServices => _sectionDIMServices;

		#endregion

		#region "Create"
		public void CreateDIM(DiagnInstrumMethods DIM)
		{
			if (DIM is null) { throw new ArgumentNullException("DIM is null", nameof(DIM)); }

			new SplitDIM(DIM, this, _httpContextAccessor, true);
		}

		#endregion

		#region "Edit"

		public void EditDIM(DiagnInstrumMethods DIM)
		{
			if (DIM is null) { throw new ArgumentNullException("DIM is null", nameof(DIM)); }

			new SplitDIM(DIM, this, _httpContextAccessor, false);
		}

		#endregion

		#region "Get"

		public List<DIMViewModel> FilterDiagnInstrumMethods(DiagnInstrumMethodsFilter filter)
		{

			List<DiagnInstrumMethods> DIM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				DIM = unitOfWork.DiagnInstrumMethods.GetDiagnInstrumMethods().ToList();
			}

			if (filter != null)
			{
				if (!string.IsNullOrEmpty(filter.CodeStudy) && filter.CodeStudy != null) { DIM = DIM.Where(e => e.CodeStudy.Contains(filter.CodeStudy)).ToList(); }
				if (filter.Id != null) { DIM = DIM.Where(e => e.id == filter.Id).ToList(); }
				if (filter.NameStude != null && !string.IsNullOrWhiteSpace(filter.NameStude)) { DIM = DIM.Where(e => e.NameStude.ToLower().Contains(filter.NameStude.ToLower())).ToList(); }
				if (filter.SectionName != null && !string.IsNullOrWhiteSpace(filter.SectionName)) { DIM = DIM.Where(e => e.SectionName.ToLower().Contains(filter.SectionName.ToLower())).ToList(); }
			}

			List<DIMViewModel> models = _mapper.Map<List<DIMViewModel>>(DIM);

			return models.ToList();
		}

		public DIMViewModel GetDIMById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			DiagnInstrumMethods DIM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				DIM = unitOfWork.DiagnInstrumMethods.GetById(id);
			}

			DIMViewModel models = _mapper.Map<DIMViewModel>(DIM);

			return models;
		}

		#endregion

		#region "Delete"

		public void DeleteDIM(int id)
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var DIM = unitOfWork.DiagnInstrumMethods.GetById(id);

				DiagnInstrumMethods models = _mapper.Map<DiagnInstrumMethods>(DIM);

				unitOfWork.DiagnInstrumMethods.Remove(models);
			}
		}

		#endregion

	}
}
