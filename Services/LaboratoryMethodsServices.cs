﻿using AutoMapper;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.abstractClasses;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class LaboratoryMethodsServices : ILaboratoryMethodsServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly ISectionLMServices _sectionLMServices;
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public LaboratoryMethodsServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, ISectionLMServices sectionLMServices, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _sectionLMServices = sectionLMServices; _httpContextAccessor = httpContextAccessor;
		}

		public IUnitOfWorkFactory GetUnitOfWorkFactory => _unitOfWorkFactory;

		public ISectionLMServices GetSectionLMServices => _sectionLMServices;

		#region "Create"

		public void CreateLM(LaboratoryMethods LM)
		{
			if (LM is null) { throw new ArgumentNullException("LM is null", nameof(LM)); }

			new SplitLM(LM, this, _httpContextAccessor, true);
		}

		#endregion

		#region "Edit"

		public void EditLM(LaboratoryMethods LM)
		{
			if (LM is null) { throw new ArgumentNullException("LM is null", nameof(LM)); }

			new SplitLM(LM, this, _httpContextAccessor, false);
		}

		#endregion

		#region "Get"

		public List<LMViewModel> FilterLaboratoryMethods(LaboratoryMethodsFilter filter)
		{

			List<LaboratoryMethods> LM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				LM = unitOfWork.LaboratoryMethods.GetLaboratoryMethods().ToList();
			}

			if (filter != null)
			{
				if (!string.IsNullOrEmpty(filter.CodeStudy) && filter.CodeStudy != null) { LM = LM.Where(e => e.CodeStudy.Contains(filter.CodeStudy)).ToList(); }
				if (filter.Id != null) { LM = LM.Where(e => e.id == filter.Id).ToList(); }
				if (filter.NameStude != null && !string.IsNullOrWhiteSpace(filter.NameStude)) { LM = LM.Where(e => e.NameStude.ToLower().Contains(filter.NameStude.ToLower())).ToList(); }
				if (filter.SectionName != null && !string.IsNullOrWhiteSpace(filter.SectionName)) { LM = LM.Where(e => e.SectionName.ToLower().Contains(filter.SectionName.ToLower())).ToList(); }
			}

			List<LMViewModel> models = _mapper.Map<List<LMViewModel>>(LM);

			return models.ToList();
		}

		public LMViewModel GetLMById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			LaboratoryMethods LM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				LM = unitOfWork.LaboratoryMethods.GetById(id);
			}

			LMViewModel models = _mapper.Map<LMViewModel>(LM);

			return models;
		}

		#endregion

		#region "Delete"

		public void DeleteLM(int id)
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var LM = unitOfWork.LaboratoryMethods.GetById(id);

				LaboratoryMethods models = _mapper.Map<LaboratoryMethods>(LM);

				unitOfWork.LaboratoryMethods.Remove(models);
			}
		}

		#endregion
	}
}
