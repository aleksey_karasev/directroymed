﻿using AutoMapper;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.abstractClasses;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class MedicRehabilitationServices : IMedicRehabilitationServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly ISectionMRServices _sectionMRServices;
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public MedicRehabilitationServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, ISectionMRServices sectionMRServices, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _sectionMRServices = sectionMRServices; _httpContextAccessor = httpContextAccessor;
		}

		public IUnitOfWorkFactory GetUnitOfWorkFactory => _unitOfWorkFactory;

		public ISectionMRServices GetSectionMRServices => _sectionMRServices;

		#region "Create"

		public void CreateMR(MedicRehabilitation MR)
		{
			if (MR is null) { throw new ArgumentNullException("MR is null", nameof(MR)); }

			new SplitMR(MR, this, _httpContextAccessor, true);
		}

		#endregion

		#region "Edit"

		public void EditMR(MedicRehabilitation MR)
		{
			if (MR is null) { throw new ArgumentNullException("MR is null", nameof(MR)); }

			new SplitMR(MR, this, _httpContextAccessor, false);
		}

		#endregion

		#region "Get"

		public List<MRViewModel> FilterMedicRehabilitation(MedicRehabilitationFilter filter)
		{

			List<MedicRehabilitation> MR;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				MR = unitOfWork.MedicRehabilitation.GetMedicRehabilitation().ToList();
			}

			if (filter != null)
			{
				if (!string.IsNullOrEmpty(filter.CodeStudy) && filter.CodeStudy != null) { MR = MR.Where(e => e.CodeStudy.Contains(filter.CodeStudy)).ToList(); }
				if (filter.Id != null) { MR = MR.Where(e => e.id == filter.Id).ToList(); }
				if (filter.NameStude != null && !string.IsNullOrWhiteSpace(filter.NameStude)) { MR = MR.Where(e => e.NameStude.ToLower().Contains(filter.NameStude.ToLower())).ToList(); }
				if (filter.SectionName != null && !string.IsNullOrWhiteSpace(filter.SectionName)) { MR = MR.Where(e => e.SectionName.ToLower().Contains(filter.SectionName.ToLower())).ToList(); }
			}

			List<MRViewModel> models = _mapper.Map<List<MRViewModel>>(MR);

			return models.ToList();
		}

		public MRViewModel GetMRById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			MedicRehabilitation MR;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				MR = unitOfWork.MedicRehabilitation.GetById(id);
			}

			MRViewModel models = _mapper.Map<MRViewModel>(MR);

			return models;
		}

		#endregion

		#region "Delete"

		public void DeleteMR(int id)
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var DIM = unitOfWork.MedicRehabilitation.GetById(id);

				MedicRehabilitation models = _mapper.Map<MedicRehabilitation>(DIM);

				unitOfWork.MedicRehabilitation.Remove(models);
			}
		}

		#endregion
	}
}
