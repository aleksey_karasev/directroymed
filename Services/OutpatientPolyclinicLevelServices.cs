﻿using AutoMapper;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.abstractClasses;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class OutpatientPolyclinicLevelServices : IOutpatientPolyclinicLevelServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly ISectionOPLServices _sectionOPLServices;
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public OutpatientPolyclinicLevelServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, ISectionOPLServices sectionOPLServices, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _sectionOPLServices = sectionOPLServices; _httpContextAccessor = httpContextAccessor;
		}

		public IUnitOfWorkFactory GetUnitOfWorkFactory => _unitOfWorkFactory;

		public ISectionOPLServices GetSectionOPLServices => _sectionOPLServices;

		#region "Create"

		public void CreateOPL(OutpatientPolyclinicLevel OPL)
		{
			if (OPL is null) { throw new ArgumentNullException("OPL is null", nameof(OPL)); }

			new SplitOPL(OPL, this, _httpContextAccessor, true);
		}

		#endregion

		#region "Edit"

		public void EditOPL(OutpatientPolyclinicLevel OPL)
		{
			if (OPL is null) { throw new ArgumentNullException("OPL is null", nameof(OPL)); }

			new SplitOPL(OPL, this, _httpContextAccessor, false);
		}

		#endregion

		#region "Get"

		public List<OPLViewModel> FilterOutpatientPolyclinicLevel(OutpatientPolyclinicLevelFilter filter)
		{

			List<OutpatientPolyclinicLevel> OPL;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				OPL = unitOfWork.OutpatientPolyclinicLevel.GetOutpatientPolyclinicLevel().ToList();
			}

			if (filter != null)
			{
				if (!string.IsNullOrEmpty(filter.CodeStudy) && filter.CodeStudy != null) { OPL = OPL.Where(e => e.CodeStudy.Contains(filter.CodeStudy)).ToList(); }
				if (filter.Id != null) { OPL = OPL.Where(e => e.id == filter.Id).ToList(); }
				if (filter.NameStude != null && !string.IsNullOrWhiteSpace(filter.NameStude)) { OPL = OPL.Where(e => e.NameStude.ToLower().Contains(filter.NameStude.ToLower())).ToList(); }
				if (filter.SectionName != null && !string.IsNullOrWhiteSpace(filter.SectionName)) { OPL = OPL.Where(e => e.SectionName.ToLower().Contains(filter.SectionName.ToLower())).ToList(); }
			}

			List<OPLViewModel> models = _mapper.Map<List<OPLViewModel>>(OPL);

			return models.ToList();
		}

		public OPLViewModel GetOPLById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			OutpatientPolyclinicLevel OPL;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				OPL = unitOfWork.OutpatientPolyclinicLevel.GetById(id);
			}

			OPLViewModel models = _mapper.Map<OPLViewModel>(OPL);

			return models;
		}

		#endregion

		#region "Delete"

		public void DeleteOPL(int id)
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var OPL = unitOfWork.OutpatientPolyclinicLevel.GetById(id);

				OutpatientPolyclinicLevel models = _mapper.Map<OutpatientPolyclinicLevel>(OPL);

				unitOfWork.OutpatientPolyclinicLevel.Remove(models);
			}
		}

		#endregion
	}
}
