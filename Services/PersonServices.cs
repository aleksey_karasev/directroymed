﻿using AutoMapper;
using Directory;
using Directory.Models;
using Directory.Models.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using Directory.Models.ModelsView;
using Directory.Services.interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Directory.Data;
using Directory.Models.Identity;
using Directory.Mails;

namespace Directory.Services
{

	///<summary>Класс описывающий сервисы</summary>
	public class PersonServices : IPersonServices
	{
		private readonly UserManager<Person> _userManager;              //Интрумент взаимодействия с пользователем
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly ICurrentPersonServices _currentPersonServices;

		///<param name="unitOfWorkFactory">Подключение к базе</param>
		///<param name="mapper">Объект осуществляющий упрощенное обращение к объектам</param>
		public PersonServices(
		IMapper mapper,
		UserManager<Person> userManager,
		ICurrentPersonServices currentPersonServices)
		{
			if (userManager is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(userManager)); }

			_mapper = mapper;
			_userManager = userManager;
			_currentPersonServices = currentPersonServices;
		}

		public void CreatePersons(PersonView person)
		{
			if (person is null) { throw new ArgumentNullException("Person is null", nameof(person)); }

			if (person.FirstName.Where(let => !char.IsLetter(let)).Count() > 0) { throw new ArgumentNullException("First name contains digit", nameof(person)); }

			if (person.LastName.Where(let => !char.IsLetter(let)).Count() > 0) { throw new ArgumentNullException("Last name contains digit", nameof(person)); }

			if (person.MiddleName.Where(let => !char.IsLetter(let)).Count() > 0) { throw new ArgumentNullException("Middle name contains digit", nameof(person)); }

			if (person.NameSystem.Where(let => !char.IsLetter(let)).Count() > 0) { throw new ArgumentNullException("System name contains digit", nameof(person)); }

			if (person.PhoneNumber.Where(let => !char.IsDigit(let)).Count() > 0) { throw new ArgumentNullException("Phone number contains letters", nameof(person)); }

			if (person.Grounding.Where(let => !char.IsLetter(let)).Count() > 0) { throw new ArgumentNullException("Grounding contains digit", nameof(person)); }

			Person persModels = _mapper.Map<Person>(person);

			_userManager.CreateAsync(persModels, person.Password);
			_userManager.CreateSecurityTokenAsync(persModels);
		}

		public void EditPersons(PersonView person)
		{
			if (person is null) { throw new ArgumentNullException("Person is null", nameof(person)); }

			Person persModels = _mapper.Map<Person>(person);

			_userManager.UpdateAsync(persModels);
		}

		///<summary>Выполняет фильтрацию ЯП</summary>
		///<param name="filter">Параметры фильтрации</param>
		public List<PersonView> FilterPerson(PersonFilter filter)
		{
			if (filter is null) { throw new ArgumentNullException("filter is null", nameof(filter)); }

			var query = _userManager.Users.AsNoTracking();

			query.AsQueryable();

			if (filter != null)
			{
				//Редактирование админа невозможно, поэтому пропускаем его в списке.
				query = query.Where(p => p.Id != "5e5d1198-3ab6-4a52-9a48-8ac816eabb00");
				//if (!string.IsNullOrWhiteSpace(filter.UserName)) { query = query.Where(p => p.UserName.ToLower().Contains(filter.UserName.ToLower())); }
				if (!string.IsNullOrWhiteSpace(filter.FirstName)) { query = query.Where(p => p.FirstName.ToLower().Contains(filter.FirstName.ToLower())); }
				if (!string.IsNullOrWhiteSpace(filter.LastName)) { query = query.Where(p => p.LastName.ToLower().Contains(filter.LastName.ToLower())); }
			}

			List<PersonView> persModels = _mapper.Map<List<PersonView>>(query);

			return persModels;
		}

		public PersonView GetPersonById(string id)
		{
			if (string.IsNullOrWhiteSpace(id)) { throw new ArgumentNullException("id is null", nameof(id)); }

			var query = _userManager.Users.AsNoTracking();

			Person per = query.FirstOrDefault(u => u.Id == id);

			PersonView persModels = _mapper.Map<PersonView>(per);

			return persModels;
		}

		public async Task ResetPassword(string id, string newPass)
		{
			if (string.IsNullOrWhiteSpace(id)) { throw new ArgumentNullException("id is null", nameof(id)); }

			var person = await _userManager.Users.FirstAsync(x => x.Id == id);

			//Отправляется сообщение пользователю
			new Mail("minzdravkr@mail.ru").Send("rnaXB5EqBW7tBhJttav7", new Mail(person.Email), $"Пароль успешно обновлен до {newPass}", "Обновление пароля");

			if (person != null)
			{
				await _userManager.RemovePasswordAsync(person);
				await _userManager.AddPasswordAsync(person, newPass);
			}
		}

		public async Task BlockUser(string userId)
		{
			if (userId is null) { throw new ArgumentNullException("userId is null", nameof(userId)); }

			var user = await _userManager.Users.FirstAsync(u => u.Id == userId);
			if (user != null)
			{
				await _userManager.SetLockoutEnabledAsync(user, true);
				await _userManager.SetLockoutEndDateAsync(user, DateTime.Today.AddYears(100));
			}
		}

		public async Task ActivateUser(string userId)
		{
			if (userId is null) { throw new ArgumentNullException("userId is null", nameof(userId)); }

			var user = await _userManager.Users.FirstAsync(u => u.Id == userId);
			if (user != null)
			{
				await _userManager.SetLockoutEnabledAsync(user, false);
				await _userManager.SetLockoutEndDateAsync(user, DateTime.Today);
			}
		}

	}
}
