﻿using AutoMapper;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.abstractClasses;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class RayСhemotherapyServices : IRayСhemotherapyServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly ISectionRCServices _sectionRCServices;
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public RayСhemotherapyServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, ISectionRCServices sectionRCServices, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _sectionRCServices = sectionRCServices; _httpContextAccessor = httpContextAccessor;
		}

		public IUnitOfWorkFactory GetUnitOfWorkFactory => _unitOfWorkFactory;

		public ISectionRCServices GetSectionRCServices => _sectionRCServices;

		#region "Create"

		public void CreateRC(RayСhemotherapy RC)
		{
			if (RC is null) { throw new ArgumentNullException("RC is null", nameof(RC)); }

			new SplitRC(RC, this, _httpContextAccessor, true);
		}

		#endregion

		#region "Edit"

		public void EditRC(RayСhemotherapy RC)
		{
			if (RC is null) { throw new ArgumentNullException("RC is null", nameof(RC)); }

			new SplitRC(RC, this, _httpContextAccessor, false);
		}

		#endregion

		#region "Get"

		public List<RCViewModel> FilterRayСhemotherapy(RayСhemotherapyFilter filter)
		{

			List<RayСhemotherapy> RC;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				RC = unitOfWork.RayСhemotherapy.GetRayСhemotherapy().ToList();
			}

			if (filter != null)
			{
				if (!string.IsNullOrEmpty(filter.CodeStudy) && filter.CodeStudy != null) { RC = RC.Where(e => e.CodeStudy.Contains(filter.CodeStudy)).ToList(); }
				if (filter.Id != null) { RC = RC.Where(e => e.id == filter.Id).ToList(); }
				if (filter.NameStude != null && !string.IsNullOrWhiteSpace(filter.NameStude)) { RC = RC.Where(e => e.NameStude.ToLower().Contains(filter.NameStude.ToLower())).ToList(); }
				if (filter.SectionName != null && !string.IsNullOrWhiteSpace(filter.SectionName)) { RC = RC.Where(e => e.SectionName.ToLower().Contains(filter.SectionName.ToLower())).ToList(); }
			}

			List<RCViewModel> models = _mapper.Map<List<RCViewModel>>(RC);

			return models.ToList();
		}

		public RCViewModel GetRCById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			RayСhemotherapy RC;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				RC = unitOfWork.RayСhemotherapy.GetById(id);
			}

			RCViewModel models = _mapper.Map<RCViewModel>(RC);

			return models;
		}

		#endregion

		#region "Delete"

		public void DeleteRC(int id)
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var RC = unitOfWork.RayСhemotherapy.GetById(id);

				RayСhemotherapy models = _mapper.Map<RayСhemotherapy>(RC);

				unitOfWork.RayСhemotherapy.Remove(models);
			}
		}

		#endregion
	}
}
