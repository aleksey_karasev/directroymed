﻿using AutoMapper;
using Directory.Data;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class SectionDIMServices : ISectionDIMServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SectionDIMServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _httpContextAccessor = httpContextAccessor;
		}

		public void CreateSection(SectionDiagnInstrum DIM)
		{
			if (DIM is null) { throw new ArgumentNullException("DIM is null", nameof(DIM)); }

			DIM.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier); ;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionDiagnInstrum.Create(DIM);
			}
		}

		public void EditSection(SectionDiagnInstrum DIM)
		{
			if (DIM is null) { throw new ArgumentNullException("DIM is null", nameof(DIM)); }

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionDiagnInstrum.Update(DIM);
			}
		}

		public List<SectionDiagnInstrum> GetSection()
		{
			List<SectionDiagnInstrum> DIM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				DIM = unitOfWork.SectionDiagnInstrum.GetSectionDIM().ToList();
			}

			return DIM.ToList();
		}

		public SectionDiagnInstrum GetSectionById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			SectionDiagnInstrum DIM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				DIM = unitOfWork.SectionDiagnInstrum.GetById(id);
			}

			return DIM;
		}

		public SelectList GetSectionWithCreate()
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var section = unitOfWork.SectionDiagnInstrum.GetSectionDIM().Where(e => e.LockedSection == false).ToList();

				return new SelectList(section, nameof(SectionDiagnInstrum.id), nameof(SectionDiagnInstrum.Name));
			}
		}
	}
}
