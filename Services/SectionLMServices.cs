﻿using AutoMapper;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class SectionLMServices : ISectionLMServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SectionLMServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _httpContextAccessor = httpContextAccessor;
		}

		public void CreateSection(SectionLaboratoryMethods LM)
		{
			if (LM is null) { throw new ArgumentNullException("LM is null", nameof(LM)); }

			LM.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier); ;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionLaboratoryMethods.Create(LM);
			}
		}

		public void EditSection(SectionLaboratoryMethods LM)
		{
			if (LM is null) { throw new ArgumentNullException("LM is null", nameof(LM)); }

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionLaboratoryMethods.Update(LM);
			}
		}

		public List<SectionLaboratoryMethods> GetSection()
		{
			List<SectionLaboratoryMethods> LM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				LM = unitOfWork.SectionLaboratoryMethods.GetSectionLM().ToList();
			}

			return LM.ToList();
		}

		public SectionLaboratoryMethods GetSectionById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			SectionLaboratoryMethods LM;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				LM = unitOfWork.SectionLaboratoryMethods.GetById(id);
			}

			return LM;
		}

		public SelectList GetSectionWithCreate()
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var section = unitOfWork.SectionLaboratoryMethods.GetSectionLM().Where(e => e.LockedSection == false).ToList();

				return new SelectList(section, nameof(SectionLaboratoryMethods.id), nameof(SectionLaboratoryMethods.Name));
			}
		}
	}
}
