﻿using AutoMapper;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class SectionMRServices : ISectionMRServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SectionMRServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _httpContextAccessor = httpContextAccessor;
		}

		public void CreateSection(SectionMedicRehabilitation MR)
		{
			if (MR is null) { throw new ArgumentNullException("MR is null", nameof(MR)); }

			MR.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier); ;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionMedicRehabilitation.Create(MR);
			}
		}

		public void EditSection(SectionMedicRehabilitation MR)
		{
			if (MR is null) { throw new ArgumentNullException("MR is null", nameof(MR)); }

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionMedicRehabilitation.Update(MR);
			}
		}

		public List<SectionMedicRehabilitation> GetSection()
		{
			List<SectionMedicRehabilitation> MR;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				MR = unitOfWork.SectionMedicRehabilitation.GetSectionMR().ToList();
			}

			return MR.ToList();
		}

		public SectionMedicRehabilitation GetSectionById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			SectionMedicRehabilitation MR;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				MR = unitOfWork.SectionMedicRehabilitation.GetById(id);
			}

			return MR;
		}

		public SelectList GetSectionWithCreate()
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var section = unitOfWork.SectionMedicRehabilitation.GetSectionMR().Where(e => e.LockedSection == false).ToList();

				return new SelectList(section, nameof(SectionMedicRehabilitation.id), nameof(SectionMedicRehabilitation.Name));
			}
		}
	}
}
