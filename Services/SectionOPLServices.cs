﻿using AutoMapper;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class SectionOPLServices : ISectionOPLServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SectionOPLServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _httpContextAccessor = httpContextAccessor;
		}

		public void CreateSection(SectionOutpatientPolyclinicLevel OPL)
		{
			if (OPL is null) { throw new ArgumentNullException("OPL is null", nameof(OPL)); }

			OPL.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier); ;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionOutpatientPolyclinicLevel.Create(OPL);
			}
		}

		public void EditSection(SectionOutpatientPolyclinicLevel OPL)
		{
			if (OPL is null) { throw new ArgumentNullException("OPL is null", nameof(OPL)); }

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionOutpatientPolyclinicLevel.Update(OPL);
			}
		}

		public List<SectionOutpatientPolyclinicLevel> GetSection()
		{
			List<SectionOutpatientPolyclinicLevel> OPL;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				OPL = unitOfWork.SectionOutpatientPolyclinicLevel.GetSectionOPL().ToList();
			}

			return OPL.ToList();
		}

		public SectionOutpatientPolyclinicLevel GetSectionById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			SectionOutpatientPolyclinicLevel OPL;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				OPL = unitOfWork.SectionOutpatientPolyclinicLevel.GetById(id);
			}

			return OPL;
		}

		public SelectList GetSectionWithCreate()
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var section = unitOfWork.SectionOutpatientPolyclinicLevel.GetSectionOPL().Where(e => e.LockedSection == false).ToList();

				return new SelectList(section, nameof(SectionOutpatientPolyclinicLevel.id), nameof(SectionOutpatientPolyclinicLevel.Name));
			}
		}
	}
}
