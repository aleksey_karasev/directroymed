﻿using AutoMapper;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class SectionRCServices : ISectionRCServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SectionRCServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _httpContextAccessor = httpContextAccessor;
		}

		public void CreateSection(SectionRayСhemotherapy RC)
		{
			if (RC is null) { throw new ArgumentNullException("OPL is null", nameof(RC)); }

			RC.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier); ;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionRayСhemotherapy.Create(RC);
			}
		}

		public void EditSection(SectionRayСhemotherapy RC)
		{
			if (RC is null) { throw new ArgumentNullException("OPL is null", nameof(RC)); }

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionRayСhemotherapy.Update(RC);
			}
		}

		public List<SectionRayСhemotherapy> GetSection()
		{
			List<SectionRayСhemotherapy> RC;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				RC = unitOfWork.SectionRayСhemotherapy.GetSectionRC().ToList();
			}

			return RC.ToList();
		}

		public SectionRayСhemotherapy GetSectionById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			SectionRayСhemotherapy OPL;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				OPL = unitOfWork.SectionRayСhemotherapy.GetById(id);
			}

			return OPL;
		}

		public SelectList GetSectionWithCreate()
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var section = unitOfWork.SectionRayСhemotherapy.GetSectionRC().Where(e => e.LockedSection == false).ToList();

				return new SelectList(section, nameof(SectionRayСhemotherapy.id), nameof(SectionRayСhemotherapy.Name));
			}
		}
	}
}
