﻿using AutoMapper;
using Directory.Models.Directoryes;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class SectionSOServices : ISectionSOServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SectionSOServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _httpContextAccessor = httpContextAccessor;
		}

		public void CreateSection(SectionSurgicalOperations SO)
		{
			if (SO is null) { throw new ArgumentNullException("SO is null", nameof(SO)); }

			SO.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier); ;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionSurgicalOperations.Create(SO);
			}
		}

		public void EditSection(SectionSurgicalOperations SO)
		{
			if (SO is null) { throw new ArgumentNullException("SO is null", nameof(SO)); }

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.SectionSurgicalOperations.Update(SO);
			}
		}

		public List<SectionSurgicalOperations> GetSection()
		{
			List<SectionSurgicalOperations> SO;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				SO = unitOfWork.SectionSurgicalOperations.GetSectionSO().ToList();
			}

			return SO.ToList();
		}

		public SectionSurgicalOperations GetSectionById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			SectionSurgicalOperations SO;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				SO = unitOfWork.SectionSurgicalOperations.GetById(id);
			}

			return SO;
		}

		public SelectList GetSectionWithCreate()
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var section = unitOfWork.SectionSurgicalOperations.GetSectionSO().Where(e => e.LockedSection == false).ToList();

				return new SelectList(section, nameof(SectionSurgicalOperations.id), nameof(SectionSurgicalOperations.Name));
			}
		}
	}
}
