﻿using AutoMapper;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Services.abstractClasses;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class SurgicalOperationsServices : ISurgicalOperationsServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IMapper _mapper;                               //Объект совмещения объектов, для упрощения
		private readonly ISectionSOServices _sectionSOServices;
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public SurgicalOperationsServices(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, ISectionSOServices sectionSOServices, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null || mapper is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _mapper = mapper; _sectionSOServices = sectionSOServices; _httpContextAccessor = httpContextAccessor;
		}

		public IUnitOfWorkFactory GetUnitOfWorkFactory => _unitOfWorkFactory;

		public ISectionSOServices GetSectionSOServices => _sectionSOServices;


		#region "Create"

		public void CreateSO(SurgicalOperations SO)
		{
			if (SO is null) { throw new ArgumentNullException("SO is null", nameof(SO)); }

			new SplitSO(SO, this, _httpContextAccessor, true);
		}

		#endregion

		#region "Edit"

		public void EditSO(SurgicalOperations SO)
		{
			if (SO is null) { throw new ArgumentNullException("SO is null", nameof(SO)); }

			new SplitSO(SO, this, _httpContextAccessor, false);
		}

		#endregion

		#region "Get"

		public List<SOViewModel> FilterSurgicalOperations(SurgicalOperationsFilter filter)
		{

			List<SurgicalOperations> SO;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				SO = unitOfWork.SurgicalOperations.GetSurgicalOperations().ToList();
			}

			if (filter != null)
			{
				if (!string.IsNullOrEmpty(filter.CodeStudy) && filter.CodeStudy != null) { SO = SO.Where(e => e.CodeStudy.Contains(filter.CodeStudy)).ToList(); }
				if (filter.Id != null) { SO = SO.Where(e => e.id == filter.Id).ToList(); }
				if (filter.NameStude != null && !string.IsNullOrWhiteSpace(filter.NameStude)) { SO = SO.Where(e => e.NameStude.ToLower().Contains(filter.NameStude.ToLower())).ToList(); }
				if (filter.SectionName != null && !string.IsNullOrWhiteSpace(filter.SectionName)) { SO = SO.Where(e => e.SectionName.ToLower().Contains(filter.SectionName.ToLower())).ToList(); }
			}

			List<SOViewModel> models = _mapper.Map<List<SOViewModel>>(SO);

			return models.ToList();
		}

		public SOViewModel GetSOById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			SurgicalOperations SO;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				SO = unitOfWork.SurgicalOperations.GetById(id);
			}

			SOViewModel models = _mapper.Map<SOViewModel>(SO);

			return models;
		}

		#endregion

		#region "Delete"

		public void DeleteSO(int id)
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var SO = unitOfWork.SurgicalOperations.GetById(id);

				SurgicalOperations models = _mapper.Map<SurgicalOperations>(SO);

				unitOfWork.SurgicalOperations.Remove(models);
			}
		}

		#endregion
	}
}
