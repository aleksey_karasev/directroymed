﻿using Directory.Models.Identity;
using Directory.Services.interfaces;
using Directory.Tokens;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class TokenServices : ITokenServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private Token _token;

		public TokenServices(IUnitOfWorkFactory unitOfWorkFactory)
		{
			if (unitOfWorkFactory == null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory;
		}

		///<summary>Создает токен, и возвращает его айди в базе</summary>
		public int CreateToken(string personId)
		{
			_token = new Token(personId);
			_token.SetToken();

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.Token.Create(_token);
			}

			return _token.id;
		}

		///<summary>Изменяет существующий токен</summary>
		public void EditToken(Token token)
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.Token.Update(token);
			}
		}

		public Token GetTokenById(int id)
		{
			Token token;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				token = unitOfWork.Token.GetById(id);
			}

			return token;
		}

		public Token GetTokenByPersonId(string id)
		{
			Token token;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				token = unitOfWork.Token.GetAll().Where(e => e.Person == id).First();
			}

			return token;
		}

		public List<Token> GetTokens()
		{
			List<Token> tokens;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				tokens = unitOfWork.Token.GetToken().ToList();
			}

			return tokens;
		}
	}
}
