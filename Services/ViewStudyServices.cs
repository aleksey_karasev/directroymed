﻿using AutoMapper;
using Directory.Models.Directoryes;
using Directory.Models.Directoryes.Additionally;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Directory.Services
{
	public class ViewStudyServices : IViewStudyServices
	{
		private readonly IUnitOfWorkFactory _unitOfWorkFactory;         //Объект подключения к БД
		private readonly IHttpContextAccessor _httpContextAccessor;     //Текущий пользователь

		public ViewStudyServices(IUnitOfWorkFactory unitOfWorkFactory, IHttpContextAccessor httpContextAccessor)
		{
			if (unitOfWorkFactory is null) { throw new ArgumentNullException("connection is null", nameof(unitOfWorkFactory)); }

			_unitOfWorkFactory = unitOfWorkFactory; _httpContextAccessor = httpContextAccessor;
		}

		public void CreateViewStudy(ViewStudy VS)
		{
			if (VS is null) { throw new ArgumentNullException("VS is null", nameof(VS)); }

			List<ViewStudy> allStudy = new();

			VS.PersonId = _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier); ;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				allStudy = unitOfWork.ViewStudy.GetViewStudy();

				if (allStudy.Where(e => e.Name == VS.Name).Count() == 0) { unitOfWork.ViewStudy.Create(VS); }
			}
		}

		public void EditViewStudy(ViewStudy VS)
		{
			if (VS is null) { throw new ArgumentNullException("VS is null", nameof(VS)); }

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				unitOfWork.ViewStudy.Update(VS);
			}
		}

		public List<ViewStudy> GetViewStudy()
		{
			List<ViewStudy> VS;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				VS = unitOfWork.ViewStudy.GetViewStudy().ToList();
			}

			return VS.ToList();
		}

		public ViewStudy GetViewStudyById(int id)
		{
			if (id < 0) { throw new ArgumentNullException("id small null", nameof(id)); }

			ViewStudy VS;

			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				VS = unitOfWork.ViewStudy.GetById(id);
			}

			return VS;
		}

		public SelectList GetViewStudyWithCreate()
		{
			//Подключаюсь к БД
			using (var unitOfWork = _unitOfWorkFactory.Create())
			{
				var section = unitOfWork.ViewStudy.GetViewStudy().Where(e => e.LockedSection == false).ToList();

				return new SelectList(section, nameof(ViewStudy.id), nameof(ViewStudy.Name));
			}
		}
	}
}
