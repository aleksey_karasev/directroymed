﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services.interfaces
{

	public interface ICurrentPersonServices
	{
		string Id { get; set; }
		string FullName { get; set; }
		bool IsRoot { get; set; }
		string DirectoryName { get; set; }
		int DirectoryId { get; set; }
	}
}
