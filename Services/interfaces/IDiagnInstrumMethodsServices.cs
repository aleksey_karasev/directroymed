﻿using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services.interfaces
{
	public interface IDiagnInstrumMethodsServices
	{
		//Список отфильтрованных медицинских справочников
		List<DIMViewModel> FilterDiagnInstrumMethods(DiagnInstrumMethodsFilter filter);

		//Создание медицинских справочников в БД
		void CreateDIM(DiagnInstrumMethods DIM);

		//Редактировать медицинских справочников
		void EditDIM(DiagnInstrumMethods DIM);

		//Поиск медицинского справочника, исходя из его идентификатора
		DIMViewModel GetDIMById(int id);

		//Поиск медицинского справочника, исходя из его идентификатора
		void DeleteDIM(int DIM);
	}
}
