﻿using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services.interfaces
{
	public interface ILaboratoryMethodsServices
	{
		//Список отфильтрованных медицинских справочников
		List<LMViewModel> FilterLaboratoryMethods(LaboratoryMethodsFilter filter);

		//Создание медицинских справочников в БД
		void CreateLM(LaboratoryMethods LM);

		//Редактировать медицинских справочников
		void EditLM(LaboratoryMethods LM);

		//Поиск медицинского справочника, исходя из его идентификатора
		LMViewModel GetLMById(int id);

		//Поиск медицинского справочника, исходя из его идентификатора
		void DeleteLM(int LM);
	}
}
