﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;

namespace Directory.Services.interfaces
{
	public interface IMedicRehabilitationServices
	{
		//Список отфильтрованных медицинских справочников
		List<MRViewModel> FilterMedicRehabilitation(MedicRehabilitationFilter filter);

		//Создание медицинских справочников в БД
		void CreateMR(MedicRehabilitation MR);

		//Редактировать медицинских справочников
		void EditMR(MedicRehabilitation MR);

		//Поиск медицинского справочника, исходя из его идентификатора
		MRViewModel GetMRById(int id);

		//Поиск медицинского справочника, исходя из его идентификатора
		void DeleteMR(int MR);
	}
}
