﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;

namespace Directory.Services.interfaces
{
	public interface IOutpatientPolyclinicLevelServices
	{
		//Список отфильтрованных медицинских справочников
		List<OPLViewModel> FilterOutpatientPolyclinicLevel(OutpatientPolyclinicLevelFilter filter);

		//Создание медицинских справочников в БД
		void CreateOPL(OutpatientPolyclinicLevel OPL);

		//Редактировать медицинских справочников
		void EditOPL(OutpatientPolyclinicLevel OPL);

		//Поиск медицинского справочника, исходя из его идентификатора
		OPLViewModel GetOPLById(int id);

		//Поиск медицинского справочника, исходя из его идентификатора
		void DeleteOPL(int OPL);
	}
}
