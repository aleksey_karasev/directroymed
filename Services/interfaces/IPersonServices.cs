﻿using Directory.Models;
using Directory.Models.Filters;
using Directory.Models.Identity;
using Directory.Models.ModelsView;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services.interfaces
{
	///<summary>Выполняет роль родителя для сервисов яп</summary>
	public interface IPersonServices
	{
		//Список отфильтрованных пользователей
		List<PersonView> FilterPerson(PersonFilter filter);

		//Создание или редактирование персоны в БД
		void CreatePersons(PersonView person);

		void EditPersons(PersonView person);

		//Поиск пользователя, исходя из роли искомого
		PersonView GetPersonById(string id);

		//Обновить пароль
		Task ResetPassword(string userId, string newPass);

		//Заблокировать пользьзователя
		Task BlockUser(string userId);

		//Разблокировать пользьзователя
		Task ActivateUser(string userId);
	}
}
