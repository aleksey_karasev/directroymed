﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;

namespace Directory.Services.interfaces
{
	public interface IRayСhemotherapyServices
	{
		//Список отфильтрованных медицинских справочников
		List<RCViewModel> FilterRayСhemotherapy(RayСhemotherapyFilter filter);

		//Создание медицинских справочников в БД
		void CreateRC(RayСhemotherapy RC);

		//Редактировать медицинских справочников
		void EditRC(RayСhemotherapy RC);

		//Поиск медицинского справочника, исходя из его идентификатора
		RCViewModel GetRCById(int id);

		//Поиск медицинского справочника, исходя из его идентификатора
		void DeleteRC(int RC);
	}
}
