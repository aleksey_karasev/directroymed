﻿using Directory.Models.Directoryes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;

namespace Directory.Services.interfaces
{
	public interface ISectionDIMServices
	{
		//Список отфильтрованных медицинских справочников
		List<SectionDiagnInstrum> GetSection();

		//Создание медицинских справочников в БД
		void CreateSection(SectionDiagnInstrum DIM);

		//Редактировать медицинских справочников
		void EditSection(SectionDiagnInstrum DIM);

		//Поиск медицинского справочника, исходя из его идентификатора
		SectionDiagnInstrum GetSectionById(int id);

		//Показать список разделов для создания DIM
		SelectList GetSectionWithCreate();
	}
}
