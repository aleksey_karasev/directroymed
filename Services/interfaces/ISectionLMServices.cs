﻿using Directory.Models.Directoryes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;

namespace Directory.Services.interfaces
{
	public interface ISectionLMServices
	{
		//Список отфильтрованных медицинских справочников
		List<SectionLaboratoryMethods> GetSection();

		//Создание медицинских справочников в БД
		void CreateSection(SectionLaboratoryMethods LM);

		//Редактировать медицинских справочников
		void EditSection(SectionLaboratoryMethods LM);

		//Поиск медицинского справочника, исходя из его идентификатора
		SectionLaboratoryMethods GetSectionById(int id);

		//Показать список разделов для создания DIM
		SelectList GetSectionWithCreate();
	}
}
