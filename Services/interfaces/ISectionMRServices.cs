﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Models.Directoryes;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Directory.Services.interfaces
{
	public interface ISectionMRServices
	{
		//Список отфильтрованных медицинских справочников
		List<SectionMedicRehabilitation> GetSection();

		//Создание медицинских справочников в БД
		void CreateSection(SectionMedicRehabilitation MR);

		//Редактировать медицинских справочников
		void EditSection(SectionMedicRehabilitation MR);

		//Поиск медицинского справочника, исходя из его идентификатора
		SectionMedicRehabilitation GetSectionById(int id);

		//Показать список разделов для создания DIM
		SelectList GetSectionWithCreate();
	}
}
