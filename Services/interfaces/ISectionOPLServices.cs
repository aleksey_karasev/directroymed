﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Models.Directoryes;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Directory.Services.interfaces
{
	public interface ISectionOPLServices
	{
		//Список отфильтрованных медицинских справочников
		List<SectionOutpatientPolyclinicLevel> GetSection();

		//Создание медицинских справочников в БД
		void CreateSection(SectionOutpatientPolyclinicLevel OPL);

		//Редактировать медицинских справочников
		void EditSection(SectionOutpatientPolyclinicLevel OPL);

		//Поиск медицинского справочника, исходя из его идентификатора
		SectionOutpatientPolyclinicLevel GetSectionById(int id);

		//Показать список разделов для создания DIM
		SelectList GetSectionWithCreate();
	}
}
