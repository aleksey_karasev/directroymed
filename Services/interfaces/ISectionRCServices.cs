﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Models.Directoryes;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Directory.Services.interfaces
{
	public interface ISectionRCServices
	{
		//Список отфильтрованных медицинских справочников
		List<SectionRayСhemotherapy> GetSection();

		//Создание медицинских справочников в БД
		void CreateSection(SectionRayСhemotherapy RC);

		//Редактировать медицинских справочников
		void EditSection(SectionRayСhemotherapy RC);

		//Поиск медицинского справочника, исходя из его идентификатора
		SectionRayСhemotherapy GetSectionById(int id);

		//Показать список разделов для создания DIM
		SelectList GetSectionWithCreate();
	}
}
