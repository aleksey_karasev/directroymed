﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Models.Directoryes;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Directory.Services.interfaces
{
	public interface ISectionSOServices
	{
		//Список отфильтрованных медицинских справочников
		List<SectionSurgicalOperations> GetSection();

		//Создание медицинских справочников в БД
		void CreateSection(SectionSurgicalOperations SO);

		//Редактировать медицинских справочников
		void EditSection(SectionSurgicalOperations SO);

		//Поиск медицинского справочника, исходя из его идентификатора
		SectionSurgicalOperations GetSectionById(int id);

		//Показать список разделов для создания DIM
		SelectList GetSectionWithCreate();
	}
}
