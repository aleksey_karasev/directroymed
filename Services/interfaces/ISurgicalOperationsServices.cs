﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.DirectoryesMedic;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;

namespace Directory.Services.interfaces
{
	public interface ISurgicalOperationsServices
	{
		//Список отфильтрованных медицинских справочников
		List<SOViewModel> FilterSurgicalOperations(SurgicalOperationsFilter filter);

		//Создание медицинских справочников в БД
		void CreateSO(SurgicalOperations SO);

		//Редактировать медицинских справочников
		void EditSO(SurgicalOperations SO);

		//Поиск медицинского справочника, исходя из его идентификатора
		SOViewModel GetSOById(int id);

		//Поиск медицинского справочника, исходя из его идентификатора
		void DeleteSO(int SO);
	}
}
