﻿using Directory.Models.Identity;
using Directory.Tokens;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Directory.Services.interfaces
{
	public interface ITokenServices
	{
		//Список токенов
		List<Token> GetTokens();

		//Создание медицинских справочников в БД
		int CreateToken(string personId);

		///<summary>Изменяет существующий токен</summary>
		public void EditToken(Token token);

		//Поиск токена, исходя из его идентификатора
		Token GetTokenById(int id);

		//Поиск токена, исходя из его идентификатора
		Token GetTokenByPersonId(string id);
	}
}
