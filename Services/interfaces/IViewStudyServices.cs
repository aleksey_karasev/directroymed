﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Models.Filters;
using Directory.Models.ModelsView.DirectoryesMedic;
using Directory.Models.Directoryes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Directory.Models.Directoryes.Additionally;

namespace Directory.Services.interfaces
{
	public interface IViewStudyServices
	{
		//Список отфильтрованных медицинских справочников
		List<ViewStudy> GetViewStudy();

		//Создание медицинских справочников в БД
		void CreateViewStudy(ViewStudy VS);

		//Редактировать медицинских справочников
		void EditViewStudy(ViewStudy VS);

		//Поиск медицинского справочника, исходя из его идентификатора
		ViewStudy GetViewStudyById(int id);

		//Показать список разделов для создания DIM
		SelectList GetViewStudyWithCreate();
	}
}
