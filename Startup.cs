using Directory.Data;
using Directory.Helper;
using Directory.ModelsConfiguraion;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Reflection;
using Directory.Services;
using System.Globalization;
using Directory.Services.interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Directory.Tokens;

namespace Directory
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
						.AddJwtBearer(options =>
						{
							options.RequireHttpsMetadata = false;
							options.TokenValidationParameters = new TokenValidationParameters
							{
								// ��������, ����� �� �������������� �������� ��� ��������� ������
								ValidateIssuer = true,
								// ������, �������������� ��������
								//ValidIssuer = AuthOptions.ISSUER,

								// ����� �� �������������� ����������� ������
								ValidateAudience = true,

								// ��������� ����������� ������
								ValidAudience = new Token().Person,

								// ����� �� �������������� ����� �������������
								ValidateLifetime = true,

								// ��������� ����� ������������
								//IssuerSigningKey = new Token().Tokens,

								// ��������� ����� ������������
								ValidateIssuerSigningKey = true,
							};
						});

			services.AddHttpClient();
			services.AddDirectoryServices(Configuration);
			services.AddApplicationDbContext(Configuration);
			services.AddControllersWithViews();

			services.AddAutoMapper(Assembly.GetExecutingAssembly());
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseMigrationsEndPoint();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");

				app.UseHsts();
			}
			app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			var cultureInfo = new CultureInfo("ru-RU");
			CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
			CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
