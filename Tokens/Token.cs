﻿using Directory.Models;
using Directory.Models.Identity;
using Directory.Services.interfaces;
using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace Directory.Tokens
{
	public class Token : Entity
	{
		public string _token;          //Стока токена
		public DateTime _dateStart;    //Дата получения токена
		public DateTime _dateEnd;      //Дата прекращения использования токена

		public string _personId;       //Пользователь владеющий токеном

		public Token() { }

		public Token(string personId) { Person = personId; }

		public string Tokens
		{
			get => _token;
			set { if (_token == string.Empty || _token == null) { _token = value; } }
		}

		public string Person
		{
			get => _personId is null || _personId == string.Empty ? "No Name" : _personId;
			set
			{
				//Если пользователь для токена еще не задан, можем задать его. Если задан, повторно задать не можем.
				if (value != null || string.IsNullOrEmpty(value) && string.IsNullOrEmpty(_personId) || _personId == null) { _personId = value; }
			}
		}

		public DateTime DateStart
		{
			get => _dateStart;
			set { if (_dateStart == new DateTime()) { _dateStart = DateTime.Now; } }
		}

		public DateTime DateEnd
		{
			get => _dateEnd;
			set
			{
				if (_dateEnd > _dateStart || _dateEnd < DateTime.Now) { _dateEnd = value; }
			}
		}

		public void SetToken()
		{
			if (_token == null)
			{

				//Солью будет длинное представление текущего времени в международном формате
				byte[] key = Encoding.ASCII.GetBytes(DateTime.UtcNow.ToString("f") + _personId);

				//Объект представляющий ключ шифрования
				SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);

				//Настройка шифра
				SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
				{
					//Какие еще данные будет хранить токен
					Subject = new ClaimsIdentity(new Claim[]{
					new Claim(ClaimTypes.Name, _personId)
				}),

					//Затем шифруем представление
					SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
				};

				JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

				JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);

				_token = handler.WriteToken(token);

				_dateStart = DateTime.Now;
			}
		}
	}
}
