﻿using System;
using Directory.Data;
using Directory.Repositories;
using Directory.Repositories.Interfaces;

namespace Directory
{
	///<summary>Характерезует совмещенное управление инструментами взимодествия со всеми таблицами</summary>
	public class UnitOfWork : IDisposable
	{
		private readonly ApplicationDbContext _context;         //Соединение с БД
		private bool disposed = false;                  //Соединение включено


		//Медицинские справочники
		private IDiagnInstrumMethodsRepository DiagnInstrumMethodsRepository;

		private ILaboratoryMethodsRepository LaboratoryMethodsRepository;

		private IMedicRehabilitationRepository MedicRehabilitationRepository;

		private IOutpatientPolyclinicLevelRepository OutpatientPolyclinicLevelRepository;

		private IRayСhemotherapyRepository RayСhemotherapyRepository;

		private ISurgicalOperationsRepository SurgicalOperationsRepository;


		//Разделы для справочников
		private ISectionDIMRepository SectionDIMRepository;

		private ISectionLMRepository SectionLMRepository;

		private ISectionMRRepository SectionMRRepository;

		private ISectionOPLRepository SectionOPLRepository;

		private ISectionRCRepository SectionRCRepository;

		private ISectionSORepository SectionSORepository;


		private IViewStudyRepository ViewStudyRepository;

		private ITokenRepository TokenRepository;

		///<summary>Выполняет создания инструментов для взаимодествия с БД</summary>
		///<param name="context">Принимает строку подключения к БД</param>
		public UnitOfWork(ApplicationDbContext context) { _context = context; }

		//Медицинские справочники

		//<summary>Возвращает БД с персонами, если обращение первое - сперва заполняет поле</summary>
		public IDiagnInstrumMethodsRepository DiagnInstrumMethods =>
			DiagnInstrumMethodsRepository is null ? (DiagnInstrumMethodsRepository = new DiagnInstrumMethodsRepository(_context)) : DiagnInstrumMethodsRepository;

		public ILaboratoryMethodsRepository LaboratoryMethods =>
			LaboratoryMethodsRepository is null ? (LaboratoryMethodsRepository = new LaboratoryMethodsRepository(_context)) : LaboratoryMethodsRepository;

		public IMedicRehabilitationRepository MedicRehabilitation =>
			MedicRehabilitationRepository is null ? (MedicRehabilitationRepository = new MedicRehabilitationRepository(_context)) : MedicRehabilitationRepository;

		public IOutpatientPolyclinicLevelRepository OutpatientPolyclinicLevel =>
		OutpatientPolyclinicLevelRepository is null ? (OutpatientPolyclinicLevelRepository = new OutpatientPolyclinicLevelRepository(_context)) : OutpatientPolyclinicLevelRepository;

		public IRayСhemotherapyRepository RayСhemotherapy =>
			RayСhemotherapyRepository is null ? (RayСhemotherapyRepository = new RayСhemotherapyRepository(_context)) : RayСhemotherapyRepository;

		public ISurgicalOperationsRepository SurgicalOperations =>
		SurgicalOperationsRepository is null ? (SurgicalOperationsRepository = new SurgicalOperationsRepository(_context)) : SurgicalOperationsRepository;


		//Разделы для справочников
		public ISectionDIMRepository SectionDiagnInstrum =>
		SectionDIMRepository is null ? (SectionDIMRepository = new SectionDIMRepository(_context)) : SectionDIMRepository;

		public ISectionLMRepository SectionLaboratoryMethods =>
		SectionLMRepository is null ? (SectionLMRepository = new SectionLMRepository(_context)) : SectionLMRepository;

		public ISectionMRRepository SectionMedicRehabilitation =>
		SectionMRRepository is null ? (SectionMRRepository = new SectionMRRepository(_context)) : SectionMRRepository;

		public ISectionOPLRepository SectionOutpatientPolyclinicLevel =>
		SectionOPLRepository is null ? (SectionOPLRepository = new SectionOPLRepository(_context)) : SectionOPLRepository;

		public ISectionRCRepository SectionRayСhemotherapy =>
		SectionRCRepository is null ? (SectionRCRepository = new SectionRCRepository(_context)) : SectionRCRepository;

		public ISectionSORepository SectionSurgicalOperations =>
		SectionSORepository is null ? (SectionSORepository = new SectionSORepository(_context)) : SectionSORepository;


		public IViewStudyRepository ViewStudy =>
			ViewStudyRepository is null ? (ViewStudyRepository = new ViewStudyRepository(_context)) : ViewStudyRepository;

		public ITokenRepository Token =>
		TokenRepository is null ? (TokenRepository = new TokenRepository(_context)) : TokenRepository;

		///<summary>Выполняет сохранение в бд</summary>
		public void Save() { _context.SaveChanges(); }

		///<summary>Отключаю соединение с БД</summary>
		public void Dispose(bool disposing) { if (disposing) { disposed = true; _context.Dispose(); } }

		///<summary>Будет ожидать отсоединения с БД</summary>
		public void Dispose() { Dispose(true); GC.SuppressFinalize(this); }
	}
}
