﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Directory.Data;

namespace Directory
{
	public interface IUnitOfWorkFactory { UnitOfWork Create(); }

	public class UnitOfWorkFactory : IUnitOfWorkFactory
	{
		private readonly IApplicationDbContextFactory _appDbContextFactory;

		public UnitOfWorkFactory(IApplicationDbContextFactory appDbContextFactory)
		{ _appDbContextFactory = appDbContextFactory; }

		public UnitOfWork Create() { return new UnitOfWork(_appDbContextFactory.Create()); }
	}
}
