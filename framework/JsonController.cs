﻿using Directory.Framework;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Directory.framework
{
	public class JsonController : Controller
	{
		protected JsonStringResult JsonData(Object jsonData)
		{
			var json = JsonConvert.SerializeObject(jsonData, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd" });

			return new JsonStringResult(json);
		}
	}
}
